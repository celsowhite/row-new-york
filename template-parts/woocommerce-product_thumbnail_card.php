<?php
/*----------------
Product Thumbnail Card
---
Used within wp loop for woocommerce products.
----------------*/
?>

<?php
global $product;
$product_price_info = rny_get_product_price_info($product);
$stock_status = $product->get_stock_status();

// Images
$product_image = get_the_post_thumbnail_url($post->ID, 'medium');
$product_image_hover = image_id_to_url(get_field('product_image_hover'), 'medium');
?>

<div class="product_thumbnail_card <?php echo $stock_status; ?>">

    <?php /* Image */ ?>

    <a 
    class="product_thumbnail_card_image thumbnail_card_image" 
    style="background-image: url('<?php echo $product_image; ?>');"
    href="<?php the_permalink(); ?>">
        <?php if($product_image_hover): ?>
            <div class="thumbnail_card_image_hover" style="background-image: url('<?php echo $product_image_hover; ?>');"></div>
        <?php endif; ?>
    </a>

    <?php /* Content */ ?>

    <div class="product_thumbnail_card_content">
        <?php /* Title */ ?>
        <p><b><a href="<?php the_permalink(); ?>"><?php if($stock_status == 'outofstock'):?>Out Of Stock - <?php endif; ?><?php the_title(); ?></a></b></p>
        <?php if($product_price_info->sale_price && $product_price_info->sale_price < $product_price_info->regular_price): ?>
            <p><del><?php echo $product_price_info->currency_symbol . $product_price_info->regular_price; ?></del> <?php echo $product_price_info->currency_symbol . $product_price_info->sale_price; ?></p>    
        <?php else: ?>
            <p><?php echo $product_price_info->currency_symbol . $product_price_info->regular_price; ?></p>    
        <?php endif; ?>
    </div>

</div>