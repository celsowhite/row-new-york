<?php 
// Conditionally set the color scheme.
if(get_sub_field('cta_bar_color') == 'black') {
    $bar_color = 'black';
    $button_color = 'yellow_border';
}
else if(get_sub_field('cta_bar_color') == 'yellow') {
    $bar_color = 'yellow';
    $button_color = 'black';
}
else if(get_sub_field('cta_bar_color') == 'blue') {
    $bar_color = 'blue';
    $button_color = 'white_border';
}
?>

<div class="call_to_action_bar <?php echo $bar_color; ?>">
    <h4><?php the_sub_field('cta_bar_text'); ?></h4>
    <a href="<?php the_sub_field('cta_bar_button_link'); ?>" class="rny_button <?php echo $button_color; ?>"><?php the_sub_field('cta_bar_button_text'); ?></a>
</div>