<?php
/*----------------
Product Title/Price
---
Custom markup for single product title/price.
This is called via includes/plugin_customizations/woocommerce.
----------------*/
?>

<?php
global $product;
$product_price_info = rny_get_product_price_info($product);
$stock_status = $product->get_stock_status();
?>

<div class="single_product_title_price_container">
    <h2 class="title"><?php the_title(); ?></h2>
    <div class="price <?php echo $stock_status; ?>">
        <?php if($product_price_info->sale_price && $product_price_info->sale_price < $product_price_info->regular_price): ?> 
            <h2 class="regular_price"><del><?php echo $product_price_info->currency_symbol . $product_price_info->regular_price; ?></del>
            <h2><?php echo $product_price_info->currency_symbol . $product_price_info->sale_price; ?></h2>
        <?php else: ?>
            <h2><?php echo $product_price_info->currency_symbol . $product_price_info->regular_price; ?><h2>
        <?php endif; ?>
    </div>
</div>