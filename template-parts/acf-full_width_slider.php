<div class="flexslider rny_slider background_image_slider <?php the_sub_field('full_width_slider_size'); ?>">
    <ul class="slides">
        <?php while(have_rows('full_width_slider_slides')): the_row(); ?>
            <li style="background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'large'); ?>'); background-position: <?php the_sub_field('horizontal_focal_point'); ?> <?php the_sub_field('vertical_focal_point'); ?> ">
                <?php if(get_sub_field('caption')): ?>
                    <p class="image_caption"><?php the_sub_field('caption'); ?></p>
                <?php endif; ?>
            </li>
        <?php endwhile; ?>
    </ul>
</div>