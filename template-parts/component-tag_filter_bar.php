<?php if(is_tag()): ?>
    
    <?php
    // Save information about this term.
    $term = get_queried_object();
    $term_id = $term->term_id;
    $term_name = $term->name;
    ?>
        
    <header class="filter_bar">
        <button class="filter_button filter_title">Filter By Tag: <span><?php echo $term_name; ?></span></button>
        <div class="filter_bar_list_container">
            <div class="container">
                <ul class="filter_bar_list">
                    <li><a href="<?php echo page_link_by_slug('blog'); ?>">All</a></li>
                    <?php
                    // Show tags in the filter bar.
                    // Order them by highest count and only pull first 16
                    $tags = get_tags(array(
                            'orderby' => 'count',
                            'order'   => 'desc'
                        )
                    );
                    $tags = array_slice($tags, 0, 16, true);
                    foreach($tags as $tag):
                    ?>
                        <?php if($tag->term_id == $term_id): ?>
                            <li class="active"><a href="<?php echo get_term_link($tag); ?>"><?php echo $tag->name; ?></a></li>
                        <?php else: ?>
                            <li><a href="<?php echo get_term_link($tag); ?>"><?php echo $tag->name; ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </header>
    
<?php endif; ?>