<?php
/*----------------
Full Width Video
---
----------------*/
?>

<div class="rny_plyr" data-plyr-provider="<?php the_sub_field('full_width_video_type'); ?>" data-plyr-embed-id="<?php the_sub_field('full_width_video_embed', false, false); ?>"></div>