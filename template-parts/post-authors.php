<?php 
/*-----------------------------
Render Post Authors
-----------------------------*/
?>

<?php 
// New method
$authors = get_field('post_authors');
if( $authors ): ?>

    <?php $author_names = array(); ?>

    <?php foreach( $authors as $author ): ?>

        <?php
            $name = get_the_title($author->ID); 
            $title = get_field('staff_title', $author->ID);
            $author_names[] = $name . ', ' . $title;
        ?>

    <?php endforeach; ?>

    <p class="post_meta">By <?php echo implode(', ', $author_names); ?></p>

<?php endif; ?>