<?php
/*----------------
Product Image Slider
---
Replaces the default product-image and product-thumbnails in woocommerce.
This is called via includes/plugin_customizations/woocommerce.
----------------*/
?>

<?php
global $product;
$featured_image_id = $product->get_image_id();
$attachment_ids = $product->get_gallery_image_ids();
?>

<div class="flexslider product_slider">
	<ul class="slides">
        <?php if($attachment_ids && has_post_thumbnail()): ?>
            <?php foreach($attachment_ids as $attachment_id): ?>
                <li><img src="<?php echo image_id_to_url($attachment_id, 'medium'); ?>" /></li>
            <?php endforeach; ?>
        <?php else: ?>
            <li><img src="<?php echo image_id_to_url($featured_image_id, 'medium'); ?>" /></li>
        <?php endif; ?>
	</ul>
</div>

<ul class="product_slider_thumbnails">
	<?php if($attachment_ids && has_post_thumbnail()): foreach($attachment_ids as $attachment_id): ?>
		<li style="background-image:url('<?php echo image_id_to_url($attachment_id, 'thumbnail'); ?>');"></li>
	<?php endforeach; endif; ?>
</ul>