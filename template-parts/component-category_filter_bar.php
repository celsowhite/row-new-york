<?php if(is_category()): ?>

    <?php
    // Save information about this term.
    $term = get_queried_object();
    $term_id = $term->term_id;
    $term_name = $term->cat_name;
    ?>
    
    <header class="filter_bar">
        <button class="filter_button filter_title">Filter By Category: <span><?php echo $term_name; ?></span></button>
        <div class="filter_bar_list_container">
            <div class="container">
                <ul class="filter_bar_list">
                    <li><a href="<?php echo page_link_by_slug('blog'); ?>">All</a></li>
                    <?php
                    $site_terms = get_terms( array(
                        'taxonomy'   => 'category',
                        'hide_empty' => false,
                    ));
                    foreach($site_terms as $site_term):
                    ?>
                        <?php if($site_term->name !== 'Uncategorized'): ?>
                            <?php if($site_term->term_id == $term_id): ?>
                                <li class="active"><a href="<?php echo get_term_link($site_term); ?>"><?php echo $site_term->name; ?></a></li>
                            <?php else: ?>
                                <li><a href="<?php echo get_term_link($site_term); ?>"><?php echo $site_term->name; ?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </header>
    
<?php endif; ?>