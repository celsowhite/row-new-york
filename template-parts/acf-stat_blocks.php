<?php
/*----------------
Stat Blocks
---
----------------*/
?>

<div class="stat_blocks">
    <?php while(have_rows('stats')): the_row(); ?>
        <div class="stat_container">
            <div class="stat">
                <h2 class="stat_percent" data-number="<?php the_sub_field('number'); ?>"></h2>    
                <svg class="stat_svg">
                    <circle cx="100" cy="100" r="94" />
                </svg>
            </div>
            <div class="stat_content">
                <p><?php the_sub_field('text'); ?></p>
            </div>
        </div>
    <?php endwhile; ?>
</div>
