<?php

/*-------------------------------------
Content Modal
---
Loaded in the header of each page.
-------------------------------------*/

?>

<div class="modal content_modal">
    <div class="modal_transparent_layer modal_close"></div>
    <div class="modal_content"></div>
    <div class="modal_close">
        <div class="close_icon_in_circle">
            <div class="close_icon"></div>
        </div>
    </div>
</div>