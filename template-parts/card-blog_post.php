<div class="post_card">
    <?php    
    // Dynamically set the featured image
    if(has_post_thumbnail()) {
        $featured_image = get_the_post_thumbnail_url($post->ID, 'medium');
    }
    else {
        $featured_image = image_id_to_url(get_field('default_post_thumbnail', 'option'), 'medium');
    }
    $featured_image_horizontal_focal_point = get_field('featured_image_horizontal_focal_point'); 
    $featured_image_vertical_focal_point = get_field('featured_image_vertical_focal_point');
    ?>
    <div class="post_card_image">
        <div class="post_card_image_block" style="background-image: url('<?php echo $featured_image; ?>'); background-position: <?php echo $featured_image_horizontal_focal_point . ' ' . $featured_image_vertical_focal_point; ?>"></div>
        <a href="<?php the_permalink(); ?>" class="post_card_image_link"></a>
    </div>
    <div class="content">
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <p>Posted <?php echo get_the_date( 'F j, Y' ); ?></p>
    </div>
</div>