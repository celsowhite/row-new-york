<?php if(have_rows('page_components')): while(have_rows('page_components')): the_row(); ?>

	<?php get_template_part('template-parts/acf', get_row_layout()); ?>

<?php endwhile; endif; ?>