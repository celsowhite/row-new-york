<?php
/*----------------
Quote Slider
---
----------------*/
?>

<div class="flexslider quote_slider <?php the_sub_field('quote_slider_color'); ?>">
    <ul class="slides">
        <?php while(have_rows('quote_slider_slides')): the_row(); ?>
            <li class="quote_slide">
                <div class="container">
                    <?php if(get_sub_field('image')): ?>
                        <div class="image" style="background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'medium'); ?>');"></div>
                    <?php endif; ?>
                    <div class="content">
                        <p><?php the_sub_field('title'); ?></p>
                        <h3><?php the_sub_field('text'); ?></h3>
                    </div>
                </div>
            </li>
        <?php endwhile; ?>
    </ul>
</div>
