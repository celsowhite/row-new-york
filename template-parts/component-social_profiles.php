<?php if(have_rows('social_profiles')): ?>
    <ul class="social_icons <?php echo $social_icon_alignment; ?>">
        <?php while(have_rows('social_profiles')): the_row(); ?>
            <li>
                <?php if(get_sub_field('network') === 'fal fa-envelope'): ?>
                    <a href="mailto:<?php the_sub_field('link'); ?>" target="_blank">
                        <i class="<?php the_sub_field('network'); ?>"></i>
                    </a>
                <?php else: ?>
                    <a href="<?php the_sub_field('link'); ?>" target="_blank">
                        <i class="<?php the_sub_field('network'); ?>"></i>
                    </a>
                <?php endif; ?>
            </li>
        <?php endwhile; ?>
    </ul>
<?php endif; ?>