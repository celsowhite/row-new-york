<div class="navigation_overlay">
		
    <div class="navigation_overlay_container">
        
        <div class="navigation_overlay_left">
                            
            <ul class="main_navigation">
                <?php 
                    // Loop through each of the menu items in the Main Menu
                    $main_menu = wp_get_nav_menu_items('main-menu');
                    foreach ($main_menu as &$menu_item) {
                        echo '<li>';
                            echo '<a href="' . $menu_item->url . '">';
                                echo '<h2>' . $menu_item->title . '</h2>';
                                echo '<p>' . $menu_item->description . '</p>';
                            echo '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
            
            <?php get_search_form(); ?>
            
        </div>
        
        <div class="navigation_overlay_right">
            <ul class="secondary_navigation">
                <?php 
                    // Loop through each of the menu items in the Secondary Menu
                    $main_menu = wp_get_nav_menu_items('secondary-menu');
                    foreach ($main_menu as &$menu_item) {
                        echo '<li>';
                            echo '<a href="' . $menu_item->url . '">';
                                echo '<h3>' . $menu_item->title . '</h3>';
                            echo '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
        </div>
                        
    </div>
    
</div>