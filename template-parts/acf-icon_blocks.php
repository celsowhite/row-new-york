<?php
/*----------------
Icon Blocks
---
----------------*/
?>

<?php if(get_sub_field('icon_blocks_background_style') === 'with_background_image'): ?>
    <div class="icon_blocks <?php the_sub_field('icon_blocks_columns'); ?> <?php the_sub_field('icon_blocks_background_style'); ?>" style="background-image:url(<?php echo image_id_to_url(get_sub_field('icon_blocks_background_image'), 'large'); ?>);">
<?php else: ?>
    <div class="icon_blocks <?php the_sub_field('icon_blocks_columns'); ?> <?php the_sub_field('icon_blocks_background_style'); ?>">
<?php endif; ?>
    <?php while(have_rows('icons')): the_row(); ?>
        <div class="icon_block animate_in_view">
            <div class="icon_container">
                <img src="<?php echo image_id_to_url(get_sub_field('icon'), 'large'); ?>" />
                <div class="icon_content">
                    <p><?php the_sub_field('text'); ?></p>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
</div>
