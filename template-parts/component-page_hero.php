<?php
/*---------------
Page Hero
---
Includes option for single image, slider or video at the top of a post/page.
---------------*/
?>

<?php 
// Image
if(get_field('hero_type') === 'image'): ?>
    <section 
        class="page_hero_image" 
        style="background-position: <?php the_field('hero_image_horizontal_focal_point'); ?> <?php the_field('hero_image_vertical_focal_point'); ?>">
        <?php if(get_field('hero_image_caption')): ?>
            <p class="image_caption"><?php the_field('hero_image_caption'); ?></p>
        <?php endif; ?>
    </section>
    <style>
        .page_hero_image {
            background-image: url('<?php echo image_id_to_url(get_field('hero_image'), 'large'); ?>');
        }
        @media all and (max-width: 1024px) {
            .page_hero_image {
                background-image: url('<?php echo image_id_to_url(get_field('hero_image'), 'medium'); ?>');
            }
        }
        @media all and (max-width: 568px) {
            .page_hero_image {
                background-image: url('<?php echo image_id_to_url(get_field('hero_image'), 'medium_large'); ?>');
            }
        }
    </style>
<?php 
// Image w/ Text Overlay
elseif(get_field('hero_type') === 'image_with_text_overlay'): ?>
    <section 
        class="page_hero_image" 
        style="background-position: <?php the_field('hero_image_horizontal_focal_point'); ?> <?php the_field('hero_image_vertical_focal_point'); ?>">
        <?php if(get_field('hero_image_caption')): ?>
            <div class="page_hero_image_overlay">
                <div class="content">
                    <?php the_field('hero_image_caption'); ?>
                </div>
            </div>
        <?php endif; ?>
    </section>
    <style>
        .page_hero_image {
            background-image: url('<?php echo image_id_to_url(get_field('hero_image'), 'large'); ?>');
        }
        @media all and (max-width: 1024px) {
            .page_hero_image {
                background-image: url('<?php echo image_id_to_url(get_field('hero_image'), 'medium'); ?>');
            }
        }
        @media all and (max-width: 568px) {
            .page_hero_image {
                background-image: url('<?php echo image_id_to_url(get_field('hero_image'), 'medium_large'); ?>');
            }
        }
    </style>
<?php 
// Slider
elseif(get_field('hero_type') === 'slider'): ?>
    <?php if( have_rows('hero_slider') ): ?>
        <div class="flexslider rny_slider page_hero_slider">
            <ul class="slides">
                <?php $slide_count=0; while( have_rows('hero_slider') ): the_row(); ?>
                    <li class="page_hero_slide_<?php echo $slide_count; ?>" style="background-position: <?php the_sub_field('horizontal_focal_point'); ?> <?php the_sub_field('vertical_focal_point'); ?>">
                        <?php if(get_sub_field('caption')): ?>
                            <p class="image_caption"><?php the_sub_field('caption'); ?></p>
                        <?php endif; ?>
                    </li>
                    <style>
                        .page_hero_slide_<?php echo $slide_count; ?>{
                            background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'large'); ?>');
                        }
                        @media all and (max-width: 1024px) {
                            .page_hero_slide_<?php echo $slide_count; ?> {
                                background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'medium'); ?>');
                            }
                        }
                        @media all and (max-width: 568px) {
                            .page_hero_slide_<?php echo $slide_count; ?> {
                                background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'medium_large'); ?>');
                            }
                        }
                    </style>
                <?php $slide_count++; endwhile; ?>
            </ul>
        </div>
    <?php endif; ?>
<?php 
// Video
elseif(get_field('hero_type') === 'video'): ?>
    <div class="rny_plyr_container">
        <div class="rny_plyr_cover_image rny_plyr_hero_image">
            <div class="play_button large white">
                <div class="triangle"></div>
            </div>
        </div>
        <div class="rny_plyr" data-plyr-provider="<?php the_field('hero_video_type'); ?>" data-plyr-embed-id="<?php the_field('hero_video_embed', false, false); ?>"></div>
    </div>
    
    <style>
        .rny_plyr_hero_image {
            background-image: url('<?php echo image_id_to_url(get_field('hero_video_cover_image'), 'large'); ?>');
        }
        @media all and (max-width: 1024px) {
            .rny_plyr_hero_image {
                background-image: url('<?php echo image_id_to_url(get_field('hero_video_cover_image'), 'medium'); ?>');
            }
        }
        @media all and (max-width: 568px) {
            .rny_plyr_hero_image {
                background-image: url('<?php echo image_id_to_url(get_field('hero_video_cover_image'), 'medium_large'); ?>');
            }
        }
    </style>
<?php 
// Video
elseif(get_field('hero_type') === 'map'): ?>
    <section class="page_hero_map">
        <?php the_field('hero_map_embed'); ?>
    </section>
<?php endif; ?>