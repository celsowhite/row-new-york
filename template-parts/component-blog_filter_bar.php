<?php if(is_page_template('page-templates/blog.php')): ?>

    <header class="filter_bar">
        <button class="filter_button filter_title">Filter By Category: <span>All</span></button>
        <div class="filter_bar_list_container">
            <div class="container">
                <ul class="filter_bar_list">
                    <li><a href="<?php echo page_link_by_slug('blog'); ?>">All</a></li>
                    <?php
                    $terms = get_terms( array(
                        'taxonomy'   => 'category',
                        'hide_empty' => false,
                    ));
                    foreach($terms as $term):
                    ?>
                        <?php if($term->name !== 'Uncategorized'): ?>
                            <li><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </header>
    
<?php endif; ?>