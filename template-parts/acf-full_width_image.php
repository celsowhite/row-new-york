<?php
/*----------------
Full Width Image
---
----------------*/
?>

<div class="full_width_image animate_in_view" style="background-image: url('<?php echo image_id_to_url(get_sub_field('full_width_image'), 'large'); ?>'); background-position: <?php the_sub_field('full_width_image_horizontal_focal_point'); ?> <?php the_sub_field('full_width_image_vertical_focal_point'); ?> ">
    <div class="full_width_image_content <?php the_sub_field('full_width_image_content_position'); ?>">
        <?php if(get_sub_field('full_width_image_subtitle')): ?>
            <p><?php the_sub_field('full_width_image_subtitle'); ?></p>
        <?php endif; ?>
        <?php if(get_sub_field('full_width_image_title')): ?>
            <h1><?php the_sub_field('full_width_image_title'); ?></h1>
        <?php endif; ?>
        <?php if(get_sub_field('full_width_image_button_link')): ?>
            <a class="rny_button black_border" href="<?php the_sub_field('full_width_image_button_link'); ?>"><?php the_sub_field('full_width_image_button_text'); ?></a>
        <?php endif; ?>
    </div>
</div>