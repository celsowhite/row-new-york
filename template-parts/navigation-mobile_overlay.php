<div class="mobile_navigation_overlay">
		
    <div class="flexslider navigation_slider">
        
        <ul class="slides">
            
            <li>
                
                <div class="mobile_primary_navigation_container">
                    <ul class="mobile_navigation_list mobile_primary_navigation">
                        <?php 
                            // Loop through each of the menu items in the Main Menu
                            $main_menu = wp_get_nav_menu_items('main-menu');
                            foreach ($main_menu as &$menu_item) {
                                echo '<li>';
                                    echo '<a href="' . $menu_item->url . '">';
                                        echo '<h2>' . $menu_item->title . '</h2>';
                                    echo '</a>';
                                echo '</li>';
                            }
                        ?>
                        <li><a href="<?php echo page_link_by_slug('contact-us'); ?>"><h2 class="text_yellow">Contact Us</h2></a></li>
                    </ul>
                    <?php get_search_form(); ?>
                </div>
                                
            </li>
            
            <li>
                <div class="mobile_secondary_navigation_container">
                    <ul class="mobile_navigation_list mobile_secondary_navigation">
                        <?php 
                            // Loop through each of the menu items in the Secondary Menu
                            $main_menu = wp_get_nav_menu_items('secondary-menu');
                            foreach ($main_menu as &$menu_item) {
                                echo '<li>';
                                    echo '<a href="' . $menu_item->url . '">';
                                        echo '<h2>' . $menu_item->title . '</h2>';
                                    echo '</a>';
                                echo '</li>';
                            }
                        ?>
                    </ul>
                </div>
            </li>
            
        </ul>
        
    </div>
    
</div>