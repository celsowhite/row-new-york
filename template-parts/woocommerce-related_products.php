<?php
/*----------------
Related Products
---
Replaces the default related-products in woocommerce.
Used as a 'More Products' section until they have a large enough amount of products to do related.
This is called via includes/plugin_customizations/woocommerce.
----------------*/
?>

<?php
global $product;
$current_product_id = $product->get_id();
?>

<section class="related products">

    <header class="section_header">
        <h4>More Products</h4>
    </header>
    
    <div class="rny_panel">

        <div class="container">

            <div class="rny_row">

                <?php
                $product_loop_args = array (
                    'post_type'         => 'product', 
                    'posts_per_page'    => 3,
                    'order'             => 'ASC',
                    'orderby'           => 'rand',
                    'post__not_in'      => array($current_product_id),
                    // Hide products that are marked as 'Exclude From Catalog'
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_visibility',
                            'field'    => 'name',
                            'terms'    => 'exclude-from-catalog',
                            'operator' => 'NOT IN',
                        ),
                    )
                );
                $product_loop = new WP_Query($product_loop_args);
                if ($product_loop -> have_posts()) : while ($product_loop -> have_posts()) : $product_loop -> the_post();
                ?>                        
                    <div class="column_1_3">
                        <?php get_template_part('template-parts/woocommerce', 'product_thumbnail_card'); ?>
                    </div>
                <?php endwhile; wp_reset_postdata(); endif; ?>

            </div>

        </div>

    </div>

</section>