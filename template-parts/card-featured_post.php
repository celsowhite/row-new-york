<div class="featured_post_card">
    <div class="image">
        <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('large'); ?>
        </a>
    </div>
    <div class="content">
        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <p class="text_grey">Posted <?php echo get_the_date( 'F j, Y' ); ?></p>
    </div>
</div>