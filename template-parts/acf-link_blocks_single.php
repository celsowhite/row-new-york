<?php
/*----------------
Link Blocks
---
Solid color blocks that link. Blocks have different color schemes and hover effects.
Black and Teal are primary colors. Hover effect can include revealing a background image.
----------------*/
?>

<div class="link_blocks <?php the_field('link_blocks_columns'); ?> <?php the_field('link_blocks_color'); ?>">
    <?php while(have_rows('link_blocks_links')): the_row(); ?>
        <?php
        // Set the appropriate class if this link block has a background image.
        if(get_sub_field('hover_image')) {
            $link_block_class = 'with_background_image';
        }
        else {
            $link_block_class = '';
        }
        ?>
        <a class="link_block <?php echo $link_block_class; ?>" href="<?php the_sub_field('link'); ?>">
            <?php if(get_sub_field('hover_image')): ?>
                <div class="background_image" style="background-image: url('<?php echo image_id_to_url(get_sub_field('hover_image'), 'medium'); ?>"></div>
            <?php endif; ?>
            <div class="content">
                <h3><?php the_sub_field('title'); ?></h3>
                <span class="separator"></span>
                <p><?php the_sub_field('text'); ?></p>
            </div>
        </a>
    <?php endwhile; ?>
</div>
