<?php
/*----------------
Shop Filter Bar
---
Shows a bar with linked terms for the shop categories. Categories are revealed in a dropdown on click.
----------------*/
?>

<?php
// Save information about this term. 
// If on the 'Shop' page then the $term_name will be an empty string.
$term = get_queried_object();
$term_id = $term->term_id;
$term_name = $term->name;

$shop_page_id = page_id_by_slug('shop');
$show_shop_categories = get_field('show_shop_categories', $shop_page_id);

?>

<?php if($show_shop_categories): ?>

    <header class="filter_bar">
        <?php if($term_name): ?>
            <button class="filter_button filter_title">Filter By Category / <span><?php echo $term_name; ?></span> <i class="far fa-angle-down"></i></button>
        <?php else: ?>
            <button class="filter_button filter_title">Filter By Category / <span>All</span> <i class="far fa-angle-down"></i></button>
        <?php endif; ?>
        <div class="filter_bar_list_container">
            <div class="container">
                <ul class="filter_bar_list">
                    <li><a href="<?php echo page_link_by_slug('shop'); ?>">All</a></li>
                    <?php
                    $terms = get_terms( array(
                        'taxonomy'   => 'product_cat',
                        'hide_empty' => false,
                    ));
                    foreach($terms as $term):
                    ?>
                        <?php if($term->name !== 'Uncategorized'): ?>
                            <?php if($term->term_id == $term_id): ?>
                                <li class="active"><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
                            <?php else: ?>
                                <li><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </header>

<?php endif; ?>
    