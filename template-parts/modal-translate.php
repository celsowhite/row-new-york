<?php

/*-------------------------------------
Translate Modal
---
Loaded in the header of each page.
-------------------------------------*/

?>

<div class="modal translate_modal">
    <div class="modal_transparent_layer modal_close"></div>
    <div class="modal_content">
        <h2 class="h1"><?php the_field('translate_overlay_title', 'option'); ?></h2>
        <div id="google_translate_element"></div><script type="text/javascript">
        function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,es,pt,ht,fr,ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
        }
        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    </div>
    <div class="modal_close">
        <div class="close_icon_in_circle">
            <div class="close_icon"></div>
        </div>
    </div>
</div>