<div class="tab_slider_section">
    
    <?php if(get_sub_field('tab_slider_header')): ?>
        <header class="section_header">
            <h4><?php the_sub_field('tab_slider_header'); ?></h4>
        </header>
    <?php endif; ?>
        
    <section class="tab_slider_container">
            
        <?php if(have_rows('tab_slides')): ?>
            <ul class="tab_navigation">
                <?php while(have_rows('tab_slides')): the_row(); ?> 
                    <li><?php the_sub_field('title'); ?></li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>

        <?php if(have_rows('tab_slides')): ?>
            <div class="tab_slider flexslider">
                <ul class="slides">
                    <?php while(have_rows('tab_slides')): the_row(); ?>
                        <li style="background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'large'); ?>');">
                            <div class="tab_slider_background_overlay"></div>
                            <div class="tab_slider_content_container">
                                <div class="tab_slider_content">
                                    <h3><?php the_sub_field('content'); ?></h3>
                                    <?php if(get_sub_field('button_link')): ?>
                                        <a href="<?php the_sub_field('button_link'); ?>" class="rny_button yellow_border"><?php the_sub_field('button_text'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        <?php endif; ?>

        <div class="tab_position_indicator"></div>

    </section>
    
</div>