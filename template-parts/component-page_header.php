<header class="page_header <?php the_field('page_header_color'); ?>">
    <div class="container">
        
        <!-- Subtitle -->
        <?php if(get_field('page_header_subtitle')): ?>
            <p><?php the_field('page_header_subtitle'); ?></p>
        <?php endif; ?>
        
        <!-- Title -->
        <?php if(get_field('page_header_title')): ?>
            <h1><?php the_field('page_header_title'); ?></h1>
        <?php else: ?>
            <h1><?php the_title(); ?></h1>
        <?php endif; ?>
        
        <!-- Link -->
        
        <?php 
        // Link is either set via the theme as built in pagination for a page/post.
        // Or the user can set the link on any backend page.
        
        if(isset($page_header_link)): ?>
            <p class="page_header_link"><a href="<?php echo $page_header_link; ?>"><?php echo $page_header_link_text; ?></a></p>
        <?php endif; ?>
        
        <?php if(get_field('page_header_link_text')): ?>
            <p class="page_header_link"><a href="<?php the_field('page_header_link_url'); ?>" <?php if(get_field('page_header_new_window')): ?>target="_blank"<?php endif; ?>><?php the_field('page_header_link_text'); ?></a></p>
        <?php endif; ?>
        
    </div>
</header>