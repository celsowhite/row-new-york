<?php 
/* --------------------------
Related Posts
--------------------------*/
?>

<?php
$related_posts_args = array (
    'post_type'      => array('post'), 
    'category__in'   => wp_get_post_categories($post->ID),
    'post__not_in'   => array($post->ID),
    'posts_per_page' => 3
);
$related_posts_loop = new WP_Query($related_posts_args);
if ($related_posts_loop -> have_posts()) :
?>	
    <header class="section_header">
        <h4>Related Posts</h4>
    </header>
    <ul class="link_blocks black three_columns">
        <?php while ($related_posts_loop -> have_posts()) : $related_posts_loop -> the_post(); ?>
            <a class="link_block with_background_image show_background_by_default" href="<?php the_permalink(); ?>">
                <div class="background_image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'medium'); ?>')"></div>
                <div class="content">
                    <h3><?php the_title(); ?></h3>
                    <span class="separator"></span>
                    <p><?php echo get_the_date( 'F j, Y' ); ?></p>
                </div>
            </a>
        <?php endwhile; wp_reset_postdata() ?>
    </div>
<?php endif; ?>
