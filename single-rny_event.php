<?php
/**
 * The template for displaying all single event posts.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page_header dark">
				<div class="container">
					<h1><?php the_title(); ?></h1>
                    <p class="page_header_link">
                        <a href="<?php echo page_link_by_slug('events'); ?>">View All Events</a>
                    </p>
				</div>
			</header>

			<div class="page_content">
                
                <?php get_template_part('template-parts/component', 'page_hero'); ?>
                
				<div class="rny_panel">
					<div class="container">
						<div class="rny_row">
                            
                            <!-- Sidebar -->
							
                            <div class="column_1_3">
                                
                                <?php if(get_field('event_primary_button_text')): ?>
                                    <p><a href="<?php the_field('event_primary_button_link'); ?>" class="rny_button yellow"><?php the_field('event_primary_button_text'); ?></a></p>
                                <?php endif; ?>
                                
                                <h4>Date</h4>
                                <p><?php the_field('event_start_date'); ?><?php if(get_field('event_end_date')): ?> - <?php the_field('event_end_date'); ?><?php endif; ?></p>
                                
                                <?php if(get_field('event_time')): ?>
                                    <h4>Time</h4>
                                    <p><?php the_field('event_time'); ?></p>
                                <?php endif; ?>
                                
                                <?php if(get_field('event_location')): ?>
                                    <h4>Location</h4>
                                    <p><?php the_field('event_location'); ?></p>
                                <?php endif; ?>
                                
                                <h4>Share</h4>
                                <ul class="social_icons rny_social_share flush_left">
                                    <?php get_template_part('template-parts/component', 'social_share'); ?>
                                </ul>
                                
							</div>
                            
                            <!-- Event Content -->
							
                            <div class="column_2_3 wysiwyg">
								<?php the_content(); ?>
							</div>
                            
						</div>
                        
					</div>

				</div>

                <?php get_template_part('template-parts/acf', 'page_components'); ?>
                
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>