<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
						
			<header class="page_header dark">
				<div class="container">
					<p>Posted <?php echo get_the_date( 'F j, Y' ); ?></p>
					<h1><?php the_title(); ?></h1>
					<p class="page_header_link"><a href="<?php echo page_link_by_slug('blog'); ?>">View All Posts</a></p>					
				</div>
			</header>
			                        
			<div class="page_content">
				
                <?php 
				if(has_post_thumbnail()):
				$featured_image_horizontal_focal_point = get_field('featured_image_horizontal_focal_point'); 
				$featured_image_vertical_focal_point = get_field('featured_image_vertical_focal_point');
				?>
	
					<section 
						class="page_hero_image" 
						style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'large') ?>'); background-position: <?php echo $featured_image_horizontal_focal_point . ' ' . $featured_image_vertical_focal_point; ?>">
					</section>
					
				<?php endif; ?>
				
				<div class="rny_panel">
					
					<div class="small_container wysiwyg">
						
						<!-- Author -->
						
						<?php get_template_part('template-parts/post', 'authors'); ?>
						
						<!-- Content -->
						
						<?php the_content(); ?>
						
						<!-- Tags -->
						
						<?php if(wp_get_post_tags($post->ID)): ?>
							<h4>Tags</h4>
							<div class="wysiwyg">
								<p>
									<?php echo category_terms_list($post->ID, 'post_tag'); ?>
								</p>
							</div>
						<?php endif; ?>
						
						<!-- Social Share -->
						
						<ul class="social_icons rny_social_share fixed">
							<?php get_template_part('template-parts/component', 'social_share'); ?>
						</ul>
						
					</div>
					
				</div>
				
				<?php get_template_part('template-parts/acf', 'page_components'); ?>
				
				<!-- Related Posts -->
				
				<?php get_template_part('template-parts/component', 'related_posts'); ?>
                                                            
			</div>

		<?php endwhile; ?>

	</main>
	
<?php get_footer(); ?>