<?php

/*================================= 
Get The Shop Page
=================================*/

function get_shop_page() {

	$args = array (
		'post_type'         => 'page', 
		'posts_per_page'    => 1,
		'pagename'          => 'shop'
	);
	
	$the_query = new WP_Query($args);

	return $the_query;

}

?>