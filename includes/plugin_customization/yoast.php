<?php

/*=============================================
Adjust Metabox Priority
=============================================*/

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

/*=============================================
Hide Annoying Yoast Notifications
=============================================*/

add_action('admin_init', 'wpc_disable_yoast_notifications');

function wpc_disable_yoast_notifications() {
  if (is_plugin_active('wordpress-seo/wp-seo.php')) {
    remove_action('admin_notices', array(Yoast_Notification_Center::get(), 'display_notifications'));
    remove_action('all_admin_notices', array(Yoast_Notification_Center::get(), 'display_notifications'));
  }
}

?>