<?php

// Connect Ajax Load More with Relevanssi

function my_alm_query_args_relevanssi($args){
   $args = apply_filters('alm_relevanssi', $args);
   return $args;
}

add_filter( 'alm_query_args_relevanssi', 'my_alm_query_args_relevanssi');

?>