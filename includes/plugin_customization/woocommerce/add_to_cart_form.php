<?php

/*----------------------
Change Add To Cart Text
----------------------*/

function woo_cart_button_text() { 

    global $product;
    $product_name = $product->get_name();

    if($product_name == 'Donate') {
        return __('Add Donation To Cart', 'woocommerce');
    }
    else {
        return __('Add to Cart', 'woocommerce');
    }

}

add_filter('woocommerce_product_single_add_to_cart_text', 'woo_cart_button_text');

/*----------------------
Add Quantity Label
----------------------*/

function woo_show_quantity_label() {
    echo '<label class="quantity_label">Quantity</label>'; 
}

add_action('woocommerce_before_add_to_cart_quantity', 'woo_show_quantity_label');

/*----------------------
After Quantity
----------------------*/

function woo_cart_buttons_wrapper() {
    echo '<div class="cart_form_buttons">';
}

add_action('woocommerce_after_add_to_cart_quantity', 'woo_cart_buttons_wrapper');

/*----------------------
Go To Cart Button next to Add To Cart Button
----------------------*/

function woo_go_to_cart_button() {
    echo '<a href="' . wc_get_cart_url() . '" class="rny_button yellow go_to_cart_button">Go To Cart</a>';
    echo '</div>';
}

add_action('woocommerce_after_add_to_cart_button', 'woo_go_to_cart_button');

/*----------------------
Donation Add To Cart Form
---
Shown on cart page.
----------------------*/

function donation_add_to_cart_form() {

	$args = array(
		'posts_per_page'      => 1,
		'post_type'           => 'product',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'no_found_rows'       => 1,
		'name'                => 'donate'
	);

	$single_product = new WP_Query($args);

	ob_start();

	global $wp_query;

	// Backup query object so following loops think this is a product page.

	$previous_wp_query = $wp_query;
	$wp_query          = $single_product;
	wp_enqueue_script('wc-single-product');
	while ( $single_product->have_posts() ) {
		$single_product->the_post()
		?>
		<div class="product donation_product no_ajax_add_to_cart">
			<?php woocommerce_template_single_add_to_cart(); ?>
		</div>
		<?php
	}

	// Restore $previous_wp_query and reset post data.

	$wp_query = $previous_wp_query;
	wp_reset_postdata();

	return ob_get_clean();

}

?>