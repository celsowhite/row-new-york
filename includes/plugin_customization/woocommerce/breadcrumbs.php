<?php

/*----------------------
Adjust Breadcrumb html output
----------------------*/

function woo_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' &#47; ',
            'wrap_before' => '<div class="rny_panel padding_top"><div class="container"><ul class="woocommerce-breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</ul></div></div>',
            'before'      => '<li>',
            'after'       => '</li>',
            'home'        => 'Shop'
        );
}

add_filter( 'woocommerce_breadcrumb_defaults', 'woo_woocommerce_breadcrumbs' );

/*----------------------
Remove uncategorized from the WooCommerce breadcrumb.
----------------------*/

function woo_remove_uncategorized_from_breadcrumb( $crumbs ) {

    // Save the 'Uncategorized' category link

	$category = get_option( 'default_product_cat' );
    $caregory_link = get_category_link( $category );

    // Loop through the array of breadcrumbs

	foreach ( $crumbs as $key => $crumb ) {

        // If the 'Uncategorized' category link is in the crumb array then remove that array item from the set of crumbs.

		if ( in_array( $caregory_link, $crumb ) ) {
			unset( $crumbs[ $key ] );
        }
        
    }

    // Return the crumbs without 'Uncategorized'

    return array_values( $crumbs );
    
}

add_filter( 'woocommerce_get_breadcrumb', 'woo_remove_uncategorized_from_breadcrumb' );

/*----------------------
Replace the home link URL
----------------------*/

function woo_breadrumb_home_url() {
    return page_link_by_slug('shop');
}

add_filter( 'woocommerce_breadcrumb_home_url', 'woo_breadrumb_home_url' );

?>