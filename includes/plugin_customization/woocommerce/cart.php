<?php

/*----------------------
Cart Collaterals
---
Add donation form and additional cart page text to the bottom of the cart page.
----------------------*/

function cart_collaterals_addition() {

    $cart_page_id = page_id_by_slug('cart');

    echo '<div class="cart-collaterals-left">';
        // Custom Donation Form
        echo donation_add_to_cart_form();
        // Additional Cart Text
        echo get_field('cart_text', $cart_page_id);
    echo '</div>';

}

add_action('woocommerce_cart_collaterals', 'cart_collaterals_addition', 8);

/*----------------------
Change 'Procceed To Checkout' Button
----------------------*/

function woocommerce_button_proceed_to_checkout() {
    // Variables
    $arrow = get_template_directory_uri() . '/img/icons/long_arrow_right_white.svg';
    $checkout_url = wc_get_checkout_url();
    // Output new button
    echo '<a href="' . $checkout_url . '"class="rny_button black full_width with_arrow">Proceed to checkout <img src="'. $arrow . '" /></a>';
}

/*----------------------
Changes the redirect URL for the Return To Shop button in the cart.
----------------------*/

function woo_empty_cart_redirect_url() {
	return page_link_by_slug('shop');
}

add_filter( 'woocommerce_return_to_shop_redirect', 'woo_empty_cart_redirect_url' );

?>