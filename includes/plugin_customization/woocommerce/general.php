<?php

/*----------------------
Woocommerce Theme Support
----------------------*/

function add_woocommerce_theme_support() {
	add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'add_woocommerce_theme_support' );

/*----------------------
Remove Woocommerce Image Sizes
---
Using custom image sizes created for the theme. No need for the 
extra bloat.
----------------------*/

function woo_remove_image_sizes() {
	remove_image_size('woocommerce_thumbnail');
	remove_image_size('woocommerce_single');
	remove_image_size('woocommerce_gallery_thumbnail');
	remove_image_size('shop_catalog');
	remove_image_size('shop_single');
	remove_image_size('shop_thumbnail');
}

add_action('init', 'woo_remove_image_sizes');

/*---------------------
Dashboard Menu Adjustments
---------------------*/

// Remove WooCommerce Variation Swatches main menu item.

function remove_woocommerce_variation_swatches_menu() {
    remove_menu_page( 'woo-variation-swatches-settings' );
}

add_action( 'admin_init', 'remove_woocommerce_variation_swatches_menu' );

// Move WooCommerce Variation Swatches to a submenu under WooCommerce

function add_woocommerce_variation_swatches_submenu() {
    add_submenu_page('woocommerce', 'Swatch Settings', 'Swatches', 'edit_theme_options', 'woo-variation-swatches-settings', ''); 
}

add_action('admin_menu', 'add_woocommerce_variation_swatches_submenu');

?>