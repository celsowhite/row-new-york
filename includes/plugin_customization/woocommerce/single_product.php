<?php

/*----------------------
Structure
----------------------*/

// Opening div for our content wrapper

function woo_shop_content_open() {
    echo '<div class="page_content">';
}

add_action('woocommerce_before_main_content', 'woo_shop_content_open', 5);

// Opening Div's for single product

function woo_single_product_summary_opening() {
    echo '<div class="container">';
    echo '<div class="rny_row">';
    echo '<div class="column_1_2">';
}

add_action('woocommerce_before_single_product_summary', 'woo_single_product_summary_opening', 5);

// Middle Div's for single product - Separating image slider from summary text

function woo_single_product_summary_middle() {
    echo '</div>';
    echo '<div class="column_1_2">';
}

add_action('woocommerce_before_single_product_summary', 'woo_single_product_summary_middle', 30);

// Closing Div's for single product

function woo_single_product_summary_end() {
    echo '</div>';
    echo '</div>';
    echo '</div>';
}

add_action('woocommerce_after_single_product_summary', 'woo_single_product_summary_end', 5);

// Closing div for our content wrapper

function woo_shop_content_close() {
    echo '</div>';
}

add_action('woocommerce_after_main_content', 'woo_shop_content_close', 50);

/*----------------------
Product Image Slider
----------------------*/

// Remove Standard Product Images

remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);

// Add custom template for the product image slider.

function woo_product_image_slider() {
    get_template_part('template-parts/woocommerce', 'product_image_slider');
};

add_action('woocommerce_before_single_product_summary', 'woo_product_image_slider');

/*----------------------
Replace excerpt on single product with full content description.
----------------------*/

// Remove the default excerpt

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);

// Add the full content to the product summary

function woo_product_content() {
    echo '<div class="wysiwyg">';
    the_content();
    echo '</div>';
};

add_action('woocommerce_single_product_summary', 'woo_product_content', 35);

/*----------------------
Remove description tabs
----------------------*/

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs');

/*----------------------
Remove product meta
----------------------*/

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

/*----------------------
Combine Title & Price Info
----------------------*/

// Remove title/price defaults

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

// Add custom title/price markup

function woo_template_title_price() {

    get_template_part('template-parts/woocommerce', 'product_title_price');

}

add_action('woocommerce_single_product_summary', 'woo_template_title_price', 10);

/*----------------------
Remove Sale Flash
----------------------*/

function woo_hide_sales_flash() {
    return false;
}

add_filter('woocommerce_sale_flash', 'woo_hide_sales_flash');

/*----------------------
Custom Related Products
----------------------*/

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

// Add custom template for the related products.

function woo_output_related_products() {
    get_template_part('template-parts/woocommerce', 'related_products');
};

add_action('woocommerce_after_single_product_summary', 'woo_output_related_products');

/*----------------------
Remove Sidebar
----------------------*/

remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

?>