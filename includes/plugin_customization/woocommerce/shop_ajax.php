<?php

/*---------------------------------
Get Cart Total
---------------------------------*/

add_action( 'wp_ajax_get_cart_total', 'get_cart_total' );
add_action( 'wp_ajax_nopriv_get_cart_total', 'get_cart_total' );

function get_cart_total() {
    echo WC()->cart->get_cart_contents_count();
    die;
}

/*---------------------------------
Add To Cart Ajax
---------------------------------*/

add_action( 'wp_ajax_add_to_cart_ajax', 'add_to_cart_ajax' );
add_action( 'wp_ajax_nopriv_add_to_cart_ajax', 'add_to_cart_ajax' );

function add_to_cart_ajax() {
    
    ob_start();

    // Product ID

    $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));

    // Quantity

    $quantity = empty( $_POST['quantity'] ) ? 1 : apply_filters( 'woocommerce_stock_amount', $_POST['quantity'] );

    // Variation Info

    $variation_id = $_POST['variation_id'];

    $variations = $_POST['variations'];

    // Validation Check

    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);

    // If passes validation and able to add to cart

    if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id, $variations)) {
        
        // Send success data back to ajax function

        $data = array(
            'cart_count' => WC()->cart->get_cart_contents_count()
        );
        
        wp_send_json_success($data);
    
    }

    // Else there was an error

    else {
        
        // If there was an error adding to the cart, redirect to the product page to show any errors

        $data = array(
            'error'       => true,
            'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id )
        );

        wp_send_json_error($data);

    }
    
    die();

}

?>