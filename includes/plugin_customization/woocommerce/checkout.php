<?php

/*----------------------
Structure
----------------------*/

// Opening div for our checkout content

function woo_customer_details_open() {
    echo '<div class="rny_row">';
    echo '<div class="column_1_2">';
}

add_action('woocommerce_checkout_before_customer_details', 'woo_customer_details_open', 5);

// Ending div for our checkout content

function woo_customer_details_end() {
    echo '</div>';
    echo '<div class="column_1_2">';
}

add_action('woocommerce_checkout_after_customer_details', 'woo_customer_details_end', 5);

// End div for order review

function woo_order_review_end() {
    echo '</div>';
    echo '</div>';
}

add_action('woocommerce_checkout_after_order_review', 'woo_order_review_end', 5);

/*----------------------
Remove Certain Checkout Fields
----------------------*/

function woo_remove_checkout_fields( $fields ) {
    // Remove billing phone number
    unset($fields['billing']['billing_phone']);
    // Remove Company Name
    unset($fields['billing']['billing_company']);
    unset($fields['shipping']['shipping_company']);
    return $fields;
}

add_filter( 'woocommerce_checkout_fields' , 'woo_remove_checkout_fields' );

/*----------------------
Change 'Place Order' Button
----------------------*/

function woo_order_button_html( $button ) {

    // Variables

    $arrow = get_template_directory_uri() . '/img/icons/long_arrow_right_white.svg';

    // Button Markup

    $button = '<button type="submit" class="rny_button black full_width with_arrow" name="woocommerce_checkout_place_order" id="place_order"><i class="fas fa-lock"></i> Place Order<img src="' . $arrow . '" /></button>';

    return $button;
}

add_filter( 'woocommerce_order_button_html', 'woo_order_button_html');

/*----------------------
Add Title Before Payment Methods
----------------------*/

function woo_payment_methods_title() {
    echo '<h3>Payment Information</h3>';
}

add_action('woocommerce_review_order_before_payment', 'woo_payment_methods_title', 5);

/*----------------------
Adjust Billing Text Strings
----------------------*/

function woo_billing_field_strings($translated_text, $text, $domain) {
    switch ($translated_text) {
        case 'Billing details' :
            $translated_text = __( 'Billing Information', 'woocommerce' );
            break;
        case 'Your order' :
            $translated_text = __( 'Your Order', 'woocommerce' );
            break;
    }
    return $translated_text;
}

add_filter('gettext', 'woo_billing_field_strings', 20, 3);

/*----------------------
Remove Coupon Form
----------------------*/

remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10); 

/*----------------------
Terms & Conditions
----------------------*/

// Stucture. Wrapping terms in wysiwyg.

function woo_terms_open() {
    echo '<div class="wysiwyg">';
}

add_action('woocommerce_checkout_terms_and_conditions', 'woo_terms_open', 5);

function woo_terms_close() {
    echo '</div>';
}

add_action('woocommerce_checkout_after_terms_and_conditions', 'woo_terms_close', 5);


?>