<?php

/*----------------------
Product Price Info
---
Given the product object return specific details about the product price.
----------------------*/

function rny_get_product_price_info($product) {

    $product_price_info = new stdClass();
    
    // Save currency symbol

    $product_price_info->currency_symbol = get_woocommerce_currency_symbol();

    // Save Regular and Sale prices depending on the product type.

    if($product->is_type('simple')) {
        $product_price_info->regular_price = $product->get_regular_price();
        $product_price_info->sale_price = $product->get_sale_price();
    } 
    elseif($product->is_type('variable')) {
        $product_price_info->regular_price = $product->get_variation_regular_price('max');
        $product_price_info->sale_price = $product->get_variation_sale_price('max');
    }

    return $product_price_info;

}

?>