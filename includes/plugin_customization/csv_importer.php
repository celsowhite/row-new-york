<?php

/*==========================================
SAVE THUMBNAIL
---
If importing a csv with the post_thumbnail column then load it into the featured image.
==========================================*/

function really_simple_csv_importer_save_thumbnail_filter( $post_thumbnail, $post, $is_update ) {

    // Import a local file from an FTP directory
    if (!empty($post_thumbnail) && file_exists($post_thumbnail)) {
        $upload_dir   = wp_upload_dir();
        $target_path  = $upload_dir['path'] . DIRECTORY_SEPARATOR . basename($post_thumbnail);
        if (copy($post_thumbnail, $target_path)) {
            $post_thumbnail = $target_path;
        }
    }

    return $post_thumbnail;
}

add_filter( 'really_simple_csv_importer_save_thumbnail', 'really_simple_csv_importer_save_thumbnail_filter', 10, 3 );

?>