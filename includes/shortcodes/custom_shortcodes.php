<?php

/*==========================================
BUTTON
==========================================*/

function rny_button_shortcode( $atts, $content = null ) {

	$a = shortcode_atts( array(
        'url'    => '',
        'color'   => 'yellow',
        'new_tab' => 'false'
    ), $atts );

    if($a['new_tab'] === 'true') {
        $new_tab = 'target="_blank"';
    }
    else {
        $new_tab = '';
    }

    return '<a href="' . $a['url'] . '"' . $new_tab . ' class="rny_button ' . $a['color'] . '">' . $content . '</a>';

}

add_shortcode( 'rny_button', 'rny_button_shortcode' );

/*==========================================
FAQ
==========================================*/

// FAQ Start

function rny_faq_start_shortcode( $atts, $content = null ) {

    return '<div class="rny_faq_container">';

}

add_shortcode('rny_faq_start', 'rny_faq_start_shortcode');

// FAQ End

function rny_faq_end_shortcode( $atts, $content = null ) {

    return '</div>';

}

add_shortcode('rny_faq_end', 'rny_faq_end_shortcode');

// FAQ Panel

function rny_faq_shortcode( $atts, $content = null ) {

    $a = shortcode_atts( array(
        'title'    => ''
    ), $atts );

    return '<div class="rny_faq"><h5 class="rny_faq_title">' . $a["title"] . '</h5><div class="rny_faq_content">' . $content . '</div></div>';

}

add_shortcode('rny_faq', 'rny_faq_shortcode');

/*==========================================
Google Calendar
==========================================*/

function rny_google_calendar_shortcode( $atts, $content = null ) {

    $a = shortcode_atts( array(
        'id'       => ''
    ), $atts );

    return '<div class="rny_google_calendar" data-id="' . $a["id"] . '"></div>';

}

add_shortcode('rny_google_calendar', 'rny_google_calendar_shortcode');

?>