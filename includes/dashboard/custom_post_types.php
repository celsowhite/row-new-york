<?php

/*==========================================
Programs
==========================================*/

// Post Type

function custom_post_type_programs() {
	$labels = array(
		'name'                => ('Programs'),
		'singular_name'       => ('Program'),
		'menu_name'           => ('Programs'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Programs'),
		'view_item'           => ('View Programs'),
		'add_new_item'        => ('Add New Programs'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Programs'),
		'update_item'         => ('Update Programs'),
		'search_items'        => ('Search Programs'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('programs'),
		'description'         => ('Programs'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'program'),
		'taxonomies'          => array('rny_programs_type'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-awards',
	);
	
	register_post_type( 'rny_program', $args );
	
}

add_action( 'init', 'custom_post_type_programs', 0 );

// Program Type

function add_program_type_taxonomy() {
	
	$labels = array(
		'name' => ('Type'),
      	'singular_name' => ('Type'),
      	'search_items' =>  ('Search Types' ),
      	'all_items' => ('All Types' ),
      	'parent_item' => ('Parent Type' ),
      	'parent_item_colon' => ('Parent Type:' ),
      	'edit_item' => ('Edit Type' ),
      	'update_item' => ('Update Type' ),
      	'add_new_item' => ('Add New Type' ),
      	'new_item_name' => ('New Type' ),
      	'menu_name' => ('Types' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array(),
	);
	
	register_taxonomy( 'rny_program_type', array('rny_program'), $args );
	
}

add_action( 'init', 'add_program_type_taxonomy', 0 );

/*==========================================
Events
==========================================*/

// Post Type

function custom_post_type_events() {
	$labels = array(
		'name'                => ('Events'),
		'singular_name'       => ('Event'),
		'menu_name'           => ('Events'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Events'),
		'view_item'           => ('View Events'),
		'add_new_item'        => ('Add New Events'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Events'),
		'update_item'         => ('Update Events'),
		'search_items'        => ('Search Events'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('events'),
		'description'         => ('Events'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'event'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-calendar-alt',
	);
	
	register_post_type( 'rny_event', $args );
	
}

add_action( 'init', 'custom_post_type_events', 0 );

/*==========================================
Staff
==========================================*/

// Post Type

function custom_post_type_staff() {
	$labels = array(
		'name'                => ('Staff'),
		'singular_name'       => ('Staff'),
		'menu_name'           => ('Staff'),
		'parent_item_colon'   => ('Staff'),
		'all_items'           => ('All Staff'),
		'view_item'           => ('View Staff'),
		'add_new_item'        => ('Add New Staff'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Staff'),
		'update_item'         => ('Update Staff'),
		'search_items'        => ('Search Staff'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('staff'),
		'description'         => ('Staff'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'staff'),
		'taxonomies'          => array('rny_staff_location'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-groups',
	);
	
	register_post_type( 'rny_staff', $args );
	
}

add_action( 'init', 'custom_post_type_staff', 0 );

// Staff Location

function add_staff_location_taxonomy() {
	
	$labels = array(
		'name' => ('Location'),
      	'singular_name' => ('Location'),
      	'search_items' =>  ('Search Locations' ),
      	'all_items' => ('All Locations' ),
      	'parent_item' => ('Parent Location' ),
      	'parent_item_colon' => ('Parent Location:' ),
      	'edit_item' => ('Edit Location' ),
      	'update_item' => ('Update Location' ),
      	'add_new_item' => ('Add New Location' ),
      	'new_item_name' => ('New Location' ),
      	'menu_name' => ('Locations' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'query_var'         => true,
		'rewrite'           => array(),
	);
	
	register_taxonomy( 'rny_staff_location', array('rny_staff'), $args );
	
}

add_action( 'init', 'add_staff_location_taxonomy', 0 );

/*==========================================
Board
==========================================*/

// Post Type

function custom_post_type_board() {
	$labels = array(
		'name'                => ('Board'),
		'singular_name'       => ('Board'),
		'menu_name'           => ('Board'),
		'parent_item_colon'   => ('Board'),
		'all_items'           => ('All Board'),
		'view_item'           => ('View Board'),
		'add_new_item'        => ('Add New Board'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Board'),
		'update_item'         => ('Update Board'),
		'search_items'        => ('Search Board'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('board'),
		'description'         => ('Board'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'board'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-businessman',
	);
	
	register_post_type( 'rny_board', $args );
	
}

add_action( 'init', 'custom_post_type_board', 0 );

/*==========================================
Forms
==========================================*/

// Post Type

function custom_post_type_forms() {
	$labels = array(
		'name'                => ('Forms'),
		'singular_name'       => ('Form'),
		'menu_name'           => ('Forms'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Forms'),
		'view_item'           => ('View Forms'),
		'add_new_item'        => ('Add New Forms'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Forms'),
		'update_item'         => ('Update Forms'),
		'search_items'        => ('Search Forms'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('forms'),
		'description'         => ('Forms'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'form'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-welcome-write-blog'
	);
	
	register_post_type( 'rny_form', $args );
	
}

add_action( 'init', 'custom_post_type_forms', 0 );

/*==========================================
Spotlight
==========================================*/

// Post Type

function custom_post_type_spotlights() {
	$labels = array(
		'name'                => ('Spotlight'),
		'singular_name'       => ('Spotlight'),
		'menu_name'           => ('Spotlights'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Spotlights'),
		'view_item'           => ('View Spotlights'),
		'add_new_item'        => ('Add New Spotlights'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Spotlights'),
		'update_item'         => ('Update Spotlights'),
		'search_items'        => ('Search Spotlights'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('spotlights'),
		'description'         => ('Spotlights'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'spotlight'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-welcome-learn-more'
	);
	
	register_post_type( 'rny_spotlight', $args );
	
}

add_action( 'init', 'custom_post_type_spotlights', 0 );

/*==========================================
JOBS
==========================================*/

// Post Type

function custom_post_type_jobs() {

	$labels = array(
		'name'                => ('Jobs'),
		'singular_name'       => ('Job'),
		'menu_name'           => ('Jobs'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Jobs'),
		'view_item'           => ('View Job'),
		'add_new_item'        => ('Add New Job'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Job'),
		'update_item'         => ('Update Job'),
		'search_items'        => ('Search Jobs'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('jobs'),
		'description'         => ('Jobs'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'job'),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-id-alt',
	);

	register_post_type( 'rny_job', $args );

}

add_action( 'init', 'custom_post_type_jobs', 0 );

/*==========================================
Press
==========================================*/

// Post Type

function custom_post_type_press() {
	$labels = array(
		'name'                => ('Press'),
		'singular_name'       => ('Press'),
		'menu_name'           => ('Press'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Press'),
		'view_item'           => ('View Press'),
		'add_new_item'        => ('Add New Press'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Press'),
		'update_item'         => ('Update Press'),
		'search_items'        => ('Search Press'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('press'),
		'description'         => ('Press'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail'),
		'hierarchical'        => false,
		'rewrite'             => array(),
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_position'       => 20,
		'menu_icon'  		  => 'dashicons-megaphone',
	);
	
	register_post_type( 'rny_press', $args );
	
}

add_action( 'init', 'custom_post_type_press', 0 );

?>