<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<main class="main_wrapper">
		
		<?php
		// Save information about this term.
		$term = get_queried_object();
		$term_slug = $term->category_nicename;
		$term_name = $term->cat_name;
		?>
		
		<!-- Filter Bar -->
		
		<header class="page_header_filter_bar">
			<button class="blog_filter_button">Filter By <span><?php echo $term_name; ?></span></button>
			<div class="filter_bar_list_container">
				<div class="container">
					<ul class="filter_bar_list">
						<?php
						$terms = get_terms( array(
							'taxonomy'   => 'category',
							'hide_empty' => false,
						));
						foreach($terms as $term):
						?>
							<li><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</header>
			
		<?php if ( have_posts() ) : ?>
			
			<div class="page_content">
				
				<div class="fixed_column_layout">
					
					<div class="fixed_column">
						
						<?php $post_count = 1; while ( have_posts() ) : the_post(); ?>

							<?php if($post_count === 1): ?>
																
								<div class="featured_post_card wysiwyg">
									<div class="image">
										<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail('large'); ?>
										</a>
									</div>
									<div class="content">
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<p class="text_grey">Posted <?php echo get_the_date( 'F j, Y' ); ?></p>
									</div>
								</div>
										
							<?php break; endif; ?>

						<?php $post_count++; endwhile; ?>
						                                    
                    </div>
					             
                    <div class="scrolled_column">
                        
                        <?php
                        // Ajax loaded posts
                        echo do_shortcode('[ajax_load_more 
                                            button_label="<a class=\'rny_button yellow\'>Load More</a>"
                                            button_loading_label="<a class=\'rny_button yellow\'>Loading</a>"
											category="' . $term_slug . '"
                                            theme_repeater="default.php" 
                                            posts_per_page="5"
											offset="1"]'
                                        ); 
                        ?>
						                                            
                    </div>
                
                </div>
                                                            
			</div>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

	</main>

<?php get_footer(); ?>
