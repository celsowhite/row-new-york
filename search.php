<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package _s
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

	 	<header class="page_header dark">
			<div class="container">
				<h1>Search</h1>
			</div>
		</header>
		
		<?php
		$term = (isset($_GET['s'])) ? $_GET['s'] : '';
		?>
		
		<div class="rny_panel">
			
			<div class="container">
					
				<!-- Search Results List -->
									
				<?php if(have_posts()): ?>
					
					<header class="search_header">
						<h2>Results for "<?php echo $term; ?>"</h2>
						<?php get_search_form(); ?>
					</header>
					
					<?php 
					
					// Ajax Load More / Relevanssi
					// AJLM takes the query args and passes them through Relevanssi do_query(). Then returns it to the DOM.
					
					echo do_shortcode('[ajax_load_more 
										id="relevanssi" 
										search="'. $term .'" 
										post_type="post, page, rny_program, rny_event"
										posts_per_page="30"
										theme_repeater="card-blog_post.php"
										transition_container_classes="blog_grid"
										button_label="<a class=\'rny_button yellow\'>Load More</a>"
										button_loading_label="<a class=\'rny_button yellow\'>Loading</a>"]'
									); 

					?>
				
				<?php else: ?>

					<h1>No results for "<?php echo $term; ?>"</h1>

				<?php endif; ?>

			</div>
			
		</div>

	</main>

<?php get_footer(); ?>
