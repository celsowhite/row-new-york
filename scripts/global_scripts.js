/*================================= 
Element In View
=================================*/

function elementInView(element) {
                                
    // Element Measurements

    const elementTop = element.offsetTop;
    const elementHeight = element.offsetHeight;
    const elementBottom = elementTop + elementHeight;
    
    // Window Measurements
    
    let windowPosition = window.scrollY;
    let windowHeight = window.innerHeight;
    let windowBottomPosition = windowPosition + windowHeight;
    
    // Check if element is in view
    
    let elementInView = windowBottomPosition >= elementTop + (elementHeight / 2);
        
    if(elementInView) {
        return true;
    }
    else {
        return false;
    }
                    
}