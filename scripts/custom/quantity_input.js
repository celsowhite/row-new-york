(function($) {

	$(document).ready(function() {

    "use strict";
        
        // Use event delegation to trigger quantity input updates.
        // In some cases, the quantity buttons are dynamically loaded on the page (i.e when the cart is updated.)
        
        document.addEventListener('click', function(e) {

            if(e.target && e.target.matches('.quantity-button')) {
                
                // Elements

                const quantityButton = e.target;
                const quantityContainer = quantityButton.closest('.quantity');
                const quantityInput = quantityContainer.querySelector('input[type=number]');

                // Save data about the input

                const quantityValue = parseInt(quantityInput.value);
                let quantityStep = quantityInput.getAttribute('step');
                quantityStep = 'undefined' !== typeof (step) ? parseInt(step) : 1;

                // Perform operations for each button type (plus/minus)

                const buttonType = quantityButton.dataset.type;

                // Save the change event so we can manually trigger the change on the input element.
                // WC has listeners waiting for the change event on the cart page to enable the 'Update Cart' button.

                const changeEvent = new Event('change', { bubbles: true });

                if (buttonType == 'minus') {
                    if (quantityValue > 1) {
                        quantityInput.value = quantityValue - quantityStep;
                        quantityInput.dispatchEvent(changeEvent);
                    }
                }
                else if (buttonType == 'plus') {
                    quantityInput.value = quantityValue + quantityStep;
                    quantityInput.dispatchEvent(changeEvent);
                }

            }

        });
    
    });

}) (jQuery);