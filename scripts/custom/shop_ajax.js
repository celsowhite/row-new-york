(function($) {

	$(document).ready(function() {

	"use strict";
        
        /*---------------------------------
        Helpers
        ---------------------------------*/
        
        /**
         * Get Variations
         *
         * @param   arr     variationInputs  -   Nodelist of the variation inputs in a form.
         * @return  object  variations       -   Formatted object of each variant name/value.
        */

        function getVariations(variationInputs) {

            let variations = {};

            // Loop through each variation input

            Array.from(variationInputs).forEach((input) => {

                const attributeName = input.getAttribute('name');
                const attributeValue = input.value;

                // Save the variation name and value in our object.

                variations[attributeName] = attributeValue;
            
            });

            return variations;

        }

        /**
         * Update Cart Icon Total
         *
         * @param   string  count  -   String number of the amount of items in the cart.
        */

        function updateCartIconTotal(count) {

            return new Promise(function(resolve, reject){

                // Elements

                const cartIconContainer = document.querySelector('.header_cart_icon');
                const cartIconNumber = document.querySelector('span.cart_icon_number');

                // Add the full cart count number to the header.

                cartIconNumber.innerHTML = count;

                // Transition in the cart icon in the header in case it was hidden (i.e no items in cart.)

                cartIconContainer.classList.remove('hide');

                resolve();

            });

        }

        /*---------------------------------
        Add To Cart
        ---------------------------------*/

        const addToCartForms = document.querySelectorAll('form.cart');

        Array.from(addToCartForms).forEach(addToCartForm => {

            // Listen for submit of an Add To Cart form

            addToCartForm.addEventListener('submit', function(e) {

                // Prevent the default page refresh

                e.preventDefault();
                
                /*---------------------------------
                Elements
                ---------------------------------*/

                const cartButtonsWrapper = this.querySelector('.cart_form_buttons');
                const addToCartButton = this.querySelector('button[type=submit]');
                const productWrapper = this.closest('.product');
                
                /*---------------------------------
                Initiate Loading State
                ---------------------------------*/

                addToCartButton.innerHTML = '<i class="fal fa-spinner"></i>';

                /*---------------------------------
                Flags
                ---------------------------------*/

                // Check if we should ajax update the page dynamically based on the form that is being submitted.
                // Ex. We don't want to ajax update the page when a donation product is being added to cart from cart page.

                const ajaxUpdate = !productWrapper.classList.contains('no_ajax_add_to_cart');
                
                /*---------------------------------
                Initiate and populate data object that will be sent to add to cart function
                ---------------------------------*/

                let data = {};

                // Action - Backend function that processes add to cart.

                data['action'] = 'add_to_cart_ajax';

                // Quantity

                data['quantity'] = this.querySelector('input[name=quantity]').value;

                // If variations product type then add all the neccesary info to data (product id, variant id and variations array.)

                if(this.classList.contains('variations_form')) {

                    // Save info about the product that is being added to cart

                    data['product_id'] = this.querySelector('input[name=product_id]').value;
                    
                    data['variation_id'] = this.querySelector('input[name=variation_id]').value;

                    const variationInputs = this.querySelectorAll('select[name*=attribute]');
                    const variations = getVariations(variationInputs);
                    data['variations'] = variations;

                }

                // Else simple product type then just add the product id to data.

                else {
                    
                    data['product_id'] = addToCartButton.value;

                }
                
                /*---------------------------------
                Send AJAX request to our backend Add To Cart function
                ---------------------------------*/

                $.ajax({
                    type: "post",
                    url: wpUrls.ajax_url,
                    data: data
                }).done(function(response) {

                    // Success

                    if(response.success){
                        
                        // If ajax update then perform actions on the page to let the user know their add to cart has been processed.

                        if(ajaxUpdate) {

                            updateCartIconTotal(response.data.cart_count);

                            // Transform the cart buttons in the form. Start by making the add to cart button 50% width.

                            cartButtonsWrapper.classList.add('reveal_enter');
                            addToCartButton.innerHTML = '<i class="far fa-check"></i>';

                            // Show the Go To Cart button

                            window.setTimeout(() => {
                                cartButtonsWrapper.classList.add('reveal_active');
                            }, 300);

                            // Revert Add to Cart button back to original text

                            window.setTimeout(() => {
                                addToCartButton.innerHTML = 'Add To Cart';
                            }, 2000);

                        }
                        // Else refresh the page.
                        else {
                            window.location.reload(true); 
                        }

                    };
                });

            });

        });

        /*---------------------------------
        Updated Cart Total
        ---------------------------------*/

        // WC fires a custom event after the cart totals have been updated.
        // We respond to that event by updating our cart icon in the header of the site.

        $(document.body).on('updated_cart_totals', function () {
            $.ajax({
                type: "post",
                url: wpUrls.ajax_url,
                data: {action: 'get_cart_total'}
            }).done(function(updatedCount) {
                updateCartIconTotal(updatedCount);
            });
        });
        
	});

})(jQuery);