(function ($) {

    $(document).ready(function () {

        "use strict";
        
        /*================================= 
		Body Loaded
		=================================*/
        
        window.setTimeout(() => {
            
            const body = document.querySelector('body');
            
            body.classList.add('loaded');
            
        }, 1000);
        
        /*================================= 
		Smooth Scroll
		=================================*/
        
        $( "body" ).on( "click", ".smooth_scroll", function(e) {

            e.preventDefault();

			// Breakpoint to adjust the offset distance.

			const tabletBreakpoint = window.matchMedia("(max-width: 768px)");

			// Get the target element & set normal offset

			let target = $(this).attr('data-target');
			let offsetDistance = -100;

			// If below our breakpoint then adjust offset for a smaller mobile header

			if (tabletBreakpoint.matches) {
				offsetDistance = -120;
			}

			target = $('#' + target);
            
			$('html, body').animate({
				scrollTop: target.offset().top + offsetDistance,
			}, 1000);
            
		});
        
        /*================================= 
		Animate In View
		=================================*/

       // Elements
        
        const animatedElements = document.querySelectorAll('.animate_in_view');
                        
        // Check position of each element on scroll and animate
        
        window.addEventListener('scroll', _.debounce(() => {
            
            if(animatedElements) {
            
                Array.from(animatedElements).forEach(animatedElement => {

                    // Check if the element has already been animated

                    const alreadyAnimated = animatedElement.classList.contains('animated');

                    // If the element is in view and hasn't already animated then animate.

                    if (elementInView(animatedElement) && !alreadyAnimated) {
                        // Animate via class
                        animatedElement.classList.add('animated');
                    }

                });
                
            }
            
        }, 30));
                
    });

})(jQuery);