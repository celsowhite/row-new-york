(function($) {

	$(document).ready(function() {

	"use strict";

        /*================================= 
		Elements
		=================================*/

        const modalTriggers = document.querySelectorAll('.modal_trigger');

        const modalClose = document.querySelectorAll('.modal_close');

        /*================================= 
		Helpers
        =================================*/
        
        // Load Content Modal

        function loadContentModal(modalTrigger, modalContainer) {

            // Elements

            const modalContentElement = modalContainer.querySelector('.modal_content');
            const modalContent = modalTrigger.querySelector('.modal_content_to_show');

            // Dynamically set the content
            
            modalContentElement.innerHTML = modalContent.innerHTML;

        }
        
        // Load Modal Video
        // If opening a video modal then add its video attributes and trigger the video to play.

        function loadModalVideo(modalTrigger, modalContainer) {

            // Elements

            const modalPlyrElement = modalContainer.querySelector('.modal_plyr_video');
            const videoType = modalTrigger.dataset.videoType;
            const videoURL = modalTrigger.dataset.videoUrl;

            // Dynamically set the Plyr DOM attributes

            modalPlyrElement.dataset.plyrProvider = videoType;
            modalPlyrElement.dataset.plyrEmbedId = videoURL;

            // Setup the plyr instance

            const modalPlyrInstance = new Plyr('.video_modal .modal_plyr_video');

            // Listen for when plyr is ready then trigger the instance to play

            modalPlyrInstance.on('ready', function (event) {
                const instance = event.detail.plyr;
                instance.play();
            });

        }

        /*================================= 
		Modal Open
		=================================*/
        
        Array.from(modalTriggers).forEach((modalTrigger) => {
            
            modalTrigger.addEventListener('click', () => {
               
               // Elements

                const modalName = modalTrigger.dataset.modal;
                const modalContainer = document.querySelector('.' + modalName);

                // Open the modal

                modalContainer.classList.add('open');
                
                // Content Modal

                if(modalName === 'content_modal') {
                    
                    loadContentModal(modalTrigger, modalContainer);
                    
                }
                
                // Video Modal
                
                if (modalName === 'video_modal') {

                    loadModalVideo(modalTrigger, modalContainer);
                
                } 
                                
            })
            
        })

        /*================================= 
		Modal Close
		=================================*/

        Array.from(modalClose).forEach(closeTrigger => {

            closeTrigger.addEventListener('click', function() {

                // Elements

                const modalContainer = this.closest('.modal');
                const modalContent = modalContainer.querySelector('.modal_content');

                // Close the modal
                
                modalContainer.classList.remove('open');

                // If closing a video modal then reset the inner DOM contents.

                if(modalContainer.classList.contains('video_modal')) {
                    modalContent.innerHTML = '<div class="modal_plyr_video" data-plyr-provider="" data-plyr-embed-id=""></div>';
                }

            })

        });

	});

})(jQuery);