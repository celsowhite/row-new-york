(function($) {

	$(document).ready(function() {

	"use strict";
    
        /*================================= 
		Default Slider
        =================================*/
        
        $('.rny_slider').flexslider({
            animation: "fade",
            keyboard: true,
            controlNav: false,
            directionNav: true,
            smoothHeight: false,
            prevText: "<i class='far fa-angle-left'></i>",
            nextText: "<i class='far fa-angle-right'></i>"
        });
        
        /*================================= 
		Navigation Slider
        =================================*/
        
        $('.navigation_slider').flexslider({
            animation: "slide",
            keyboard: true,
            controlNav: true,
            directionNav: false,
            slideshow: false
        });
        
        /*================================= 
		Quote Slider
        =================================*/

        $('.quote_slider').flexslider({
            animation: "fade",
            keyboard: true,
            controlNav: false,
            directionNav: true,
            prevText: "<i class='far fa-angle-left'></i>",
            nextText: "<i class='far fa-angle-right'></i>"
        });

        /*================================= 
		Product Slider
        =================================*/

        $('.product_slider').flexslider({
            animation: "slide",
            keyboard: true,
            controlNav: true,
            manualControls: ".product_slider_thumbnails li",
            directionNav: false,
            prevText: "<i class='far fa-angle-left'></i>",
            nextText: "<i class='far fa-angle-right'></i>",
            slideshow: false
        });
        
		/*================================= 
		Fullscreen Slider
        =================================*/
        
        // Elements
        
        const fullscreenSliderTabs = document.querySelectorAll('.fullscreen_slider_tab');
        
        const fullscreenSliderSlides = document.querySelectorAll('.fullscreen_slider ul.slides > li');
        
        // Helpers
        
        function hideActiveTab() {
            
            const activeTab = document.querySelector('.fullscreen_slider_tab.active');

            activeTab.classList.remove('active');
            
        }
        
        // Setup the slider
              
        $('.fullscreen_slider').flexslider({
            animation: "fade",
            keyboard: true,
            controlNav: false,
            directionNav: false,
            animationSpeed: 300,
            slideshowSpeed: 5000,
            before: function(slider) {
                hideActiveTab();
                // Add the active class to the next slides corresponding active tab
                const nextSlideIndex = slider.animatingTo;
                fullscreenSliderTabs[nextSlideIndex].classList.add('active');
            }
        });
        
        // Tab Hover
                
        if(fullscreenSliderTabs.length !== 0) {
                        
            // Initially set the first tab to active

            fullscreenSliderTabs[0].classList.add('active');

            // Tab Hover Effect
            
            let fullscreenSliderThrottle;
            
            Array.from(fullscreenSliderTabs).forEach((tab) => {

                // When hovering into one of the slider tabs then trigger the slider to change.

                tab.addEventListener('mouseenter', () => {

                    hideActiveTab();

                    // Add the active class to the new tab

                    tab.classList.add('active');

                    // Get the position of this tab

                    const tabPosition = Array.from(fullscreenSliderTabs).indexOf(tab);
                    
                    // Move the slider to that same position.
                    // Need to stop it first in case it's in the middle of another animation.
                    
                    $('.fullscreen_slider').flexslider("stop")
                    $('.fullscreen_slider').flexslider(tabPosition);
                    
                    // Clear the previous throttle timeout from executing.
                    
                    clearTimeout(fullscreenSliderThrottle);
                    
                    // Set a new timeout that will trigger flexslider to change positions.
                    // Ensures it's not called during a running animation because animations take 300ms and this runs after 600ms
                    
                    fullscreenSliderThrottle = setTimeout(function(){
                        $('.fullscreen_slider').flexslider(tabPosition);
                    }, 600)

                });

            });
            
        }
        
        // Lazy load fullscreen slider background images
        // First image is already loaded into the DOM.
        // We wait to load the hidden slides.
        
        if(fullscreenSliderSlides.length !== 0) {
        
            window.setTimeout(function() {
                Array.from(fullscreenSliderSlides).forEach(slide => {
                    slide.classList.add('loaded');
                })
            }, 2000)
            
        }
        
        /*================================= 
		Thumbnail Controlled Slider
        =================================*/
        
        $('.thumbnail_slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: true,
            slideshow: false,
            prevText: "<i class='far fa-angle-left'></i>",
            nextText: "<i class='far fa-angle-right'></i>",
            itemWidth: 400,
            itemMargin: 5,
            asNavFor: '.thumbnail_controlled_slider'
        });
          
         $('.thumbnail_controlled_slider').flexslider({
            animation: "fade",
            controlNav: false,
            directionNav: false,
            slideshowSpeed: 2000,
            slideshow: false,
            sync: ".thumbnail_slider",
            after: function(slider){
                $('.tabs_container').tabify.activateTab(slider.animatingTo);
            }
        });
                
        /*================================= 
        Tab Slider
        =================================*/
        
        // Elements
        
        const sliderTab = $('.tab_navigation li');
        const firstTab = $('.tab_navigation li:first-child');
        
        // Helpers
        
        function moveTabIndicator(nextTab) {
            
            // Elements
            
            const tabIndicator = nextTab.closest('.tab_slider_container').find('.tab_position_indicator');
            const activeTab = nextTab.closest('.tab_slider_container').find('.tab_navigation li.active');
           
            // Make the next tab active
            
            activeTab.removeClass('active');
            nextTab.addClass('active');
            
            // Get the position of the next tab relative to the parent element (ul.tab_navigation).
            
            const nextTabPosition = nextTab.position().top;
                        
            // Animate the indicator to the next tab position
            
            tabIndicator.css({"top": nextTabPosition + 5});
            
        }
                
        // Initialize flexslider

        $('.tab_slider').flexslider({
            animation: "fade",
            controlNav: false,
            directionNav: false,
            slideshowSpeed: 3000,
            slideshow: true,
            touch: false,
            useCSS: false,
            before: function(slider) {
                // Upon animating to the next slide, move the tab indicator to the corresponding position
                const nextTabIndex = slider.animatingTo;
                const nextTab = sliderTab.eq(nextTabIndex);
                moveTabIndicator(nextTab);
            }
        });

        // Activate the first tab on page load.
        
        firstTab.each(function() {
            
            moveTabIndicator($(this));
                        
        });
                    
        // Click action on tab items

        sliderTab.bind('click touchstart', function(){
            
            // Animate the indicator to that position. 
            
            moveTabIndicator($(this));
            
            // Animate the slider to the corresponding slide.  
                              
            const tabIndex = $(this).index();
            
            const tabSlider = $(this).closest('.tab_slider_container').find('.tab_slider');

            tabSlider.flexslider(tabIndex);
            
        });
        
	});

})(jQuery);