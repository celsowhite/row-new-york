(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Trigger the additional groups tabs.
		=================================*/

        document.addEventListener('click', function (e) {

            // Elements

            const wwaTabTrigger = e.target.closest('.wwa-tab-trigger');

            // If clicked on a tab trigger then find the tab and make it active.

            if (wwaTabTrigger) {

                // Save all of the tab items

                const tabItems = document.querySelectorAll('.tab_items li');

                // Get the tab name we want to trigger

                const tabName = wwaTabTrigger.dataset.tabName;

                // Get the index of the tab we want to trigger.

                const tabIndex = Array.from(tabItems).findIndex(function(item){
                    // The name on the tab trigger won't always be exactly what is on the tab item.
                    // Therefore we check for the presence of it.
                    return item.dataset.name.includes(tabName);
                });

                // Activate the tab programatically.

                $('.tabs_container').tabify.activateTab(tabIndex);
                
            }

        });

	});

})(jQuery);