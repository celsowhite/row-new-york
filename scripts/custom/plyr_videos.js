(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Plyr
		=================================*/
		
		// Elements
		
		const rnyPlyrElements = document.querySelectorAll('.rny_plyr');
		const rnyPlyrContainers = document.querySelectorAll('.rny_plyr_container');
		
		// Initialize Plyrs on the page
		
		const rnyPlyrs = Array.from(rnyPlyrElements).map(p => new Plyr(p));
				
		/*================================= 
		Custom Play Trigger
		=================================*/
		
		// Elements
		
		const playButtons = document.querySelectorAll('.rny_plyr_container .play_button');
		
		// Click event on each Plyr play button
		
		Array.from(playButtons).forEach(playButton => {
			
			playButton.addEventListener('click', () => {
				
				// Get the related plyr container elements
				
				const plyrContainer = playButton.closest('.rny_plyr_container');
				const plyrCoverImage = plyrContainer.querySelector('.rny_plyr_cover_image');
				const plyrElement = plyrContainer.querySelector('.plyr');
				
				// Get Index of this Plyr on the page
				
				const plyrIndex = Array.from(rnyPlyrContainers).indexOf(plyrContainer);
											
				// Play this video instance
				
				rnyPlyrs[plyrIndex].play();
				
				// Hide the cover image

				window.setTimeout(function(){
										
					plyrCoverImage.classList.add('hide');
				
				}, 1000);
				
			});
			
		});

	});

})(jQuery);