(function ($) {

    $(document).ready(function () {

        "use strict";
        
        /*================================= 
		Animated Percent Circle
		=================================*/

        // Elements
        
        const stats = document.querySelectorAll('.stat');
                
        // Constants
        
        let canvasSize = 200;
        let strokeWidth = 10;
        let strokeColor = '#FFE657';
        let centre = canvasSize / 2;
        let radius = Math.floor((canvasSize - strokeWidth) / 2);
        let startY = centre - radius;
        
        // Run Animation
        
        function runSvgAnimation(statElement) {
            
            // Elements

            let percentDiv = statElement.querySelector('.stat_percent');
            let percent = percentDiv.dataset.number / 100;
            let statSvg = statElement.querySelector('svg.stat_svg');

            // Snap Initialization

            let s = Snap(statSvg);
            let path = '';
            let arc = s.path(path);

            // Constants

            let endpoint = percent * 360;

            // Show Circle

            statSvg.classList.add('show_circle');

            // Animate Circle

            Snap.animate(0, endpoint, function (val) {

                // Calculations

                const d = val;
                const dr = d - 90;
                const radians = Math.PI * (dr) / 180;
                const endX = centre + radius * Math.cos(radians);
                const endY = centre + radius * Math.sin(radians);
                const largeArc = d > 180 ? 1 : 0;

                // Animate the SVG path

                const path = "M" + centre + "," + startY + " A" + radius + "," + radius + " 0 " + largeArc + ",1 " + endX + "," + endY;
                const arc = s.path(path);

                arc.attr({
                    stroke: strokeColor,
                    fill: 'none',
                    strokeWidth: strokeWidth
                });

                // Increment the percent text.

                percentDiv.innerHTML = Math.round(val / 360 * 100) + '%';

            }, 2000, mina.easeinout);

        }
        
        // Check position of each stat on scroll and animate
        
        window.addEventListener('scroll', _.debounce(() => {
            
            if(stats) {
            
                Array.from(stats).forEach(stat => {

                    // Check if the stat has already been animated

                    const alreadyAnimated = stat.classList.contains('animated');

                    // If the stat is in view and hasn't already animated then animate.

                    if (elementInView(stat) && !alreadyAnimated) {
                        // Run the animation on the stat
                        runSvgAnimation(stat);
                        // Indicate that it has already animated.
                        stat.classList.add('animated');
                    }

                });
                
            }
            
        }, 30));
                
    });

})(jQuery);