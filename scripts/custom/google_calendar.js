(function($) {

	$(document).ready(function() {

	"use strict";
                        
        /*================================= 
		Initialize Calendars
		=================================*/
        
        const googleCalendars = $('.rny_google_calendar');
        
        googleCalendars.each(function() {
            
            // Save the ID of this calendar
            
            const calId = $(this).data('id');
            
            const pageTitle = document.querySelector('.page_header .container h1');
            
            // Initialize it
            
            $(this).fullCalendar({
                googleCalendarApiKey: 'AIzaSyA08prPcRF6Q_66JOCDI57iVtVm-q4QVPM',
                events: {
                    googleCalendarId: calId,
                },
                header: {
                    left: 'prev,next',
                    center: 'title',
                    right: 'month,basicWeek'
                },
                buttonIcons: {
                    prev: 'left-single-arrow',
                    next: 'right-single-arrow',
                    prevYear: 'left-double-arrow',
                    nextYear: 'right-double-arrow'
                },
                eventMouseover: function( event, jsEvent, view ) {         
                    /* const instance = new Tooltip($(this)[0], {
                        title: event.title,
                        trigger: 'hover',
                        placement: 'top',
                        template: '<div class="tooltip popper_tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
                    }); */
                }
            });
            
        });
        
	});

})(jQuery);
