(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Translate Detect
		=================================*/

		function translateDetect() {
            
            // Elements
            
            const googleTranslateIframe = document.querySelector('iframe.goog-te-banner-frame');
            const translateToggle = document.querySelector('.translate_toggle');
            
            if(googleTranslateIframe) {
                translateToggle.classList.add('on');
            }
            
        }
        
        window.setTimeout(translateDetect, 3000);

	});

})(jQuery);