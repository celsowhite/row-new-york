(function($) {

	$(document).ready(function() {

	"use strict";
        
        /*================================= 
        MixItUp
        Filtering of grid items. Used for staff.
        https://www.kunkalabs.com/mixitup/docs/get-started/
        =================================*/

        if($('.mixitup_container').length) {
            
            const mixer = mixitup('.mixitup_container', {
                animation: {
                    duration: 600,
                    reverseOut: false,
                    effects: "fade"
                }
            });
            
            // Save the URL parameter info.
                   
            const baseURL = window.location.href.split('?')[0];
            const urlParams = new URLSearchParams(window.location.search);
            const urlParamLocation = urlParams.get('location');
            
            // Filter the grid on page load if the location is set as a URL param.
            
            if(urlParamLocation) {
                mixer.filter('.' + urlParamLocation);
            }
            
            // Change the location url parameter upon clicking a filter
            
            const filters = document.querySelectorAll('ul.sub_nav li.filter');
            
            Array.from(filters).forEach(filter => {
                
                filter.addEventListener('click', () => {

                    // Save the filter name
                    
                    let filterName = filter.dataset.filter;
                    
                    // Check if the filter name has a period at the beginning.
                    // Mixitup requires the filter data attribute to have a '.' before it.
                    
                    if(filterName.charAt(0) === '.') {
                        
                        filterName = filterName.substr(1);
                        
                        // Set the location url parameter
                    
                        urlParams.set('location', filterName);
                        
                        // Update the URL in the browser
                    
                        window.history.replaceState({}, '', baseURL + '?' + urlParams);
                        
                    }
                    
                    // Else then the 'all' filter 
                    
                    else {
                        
                        urlParams.delete('location')
                        
                        // Update the URL in the browser
                    
                        window.history.replaceState({}, '', baseURL);
                        
                    }
                                        
                    
                });
                
            });          
                                    
        };
        
    });

})(jQuery);