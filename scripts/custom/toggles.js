(function($) {

	$(document).ready(function() {

	"use strict";
    
        /*================================= 
        FAQ Expand
        =================================*/

        // Find all of the FAQ's on the page.

        const rnyFaq = document.querySelectorAll('.rny_faq');

        Array.from(rnyFaq).forEach((faq) => {

            // Add a click event listener to each.

            faq.addEventListener('click', (e) => {

                // Toggle the open state on click.

                faq.classList.toggle('open');

            });

        });
        
        /*================================= 
        Filter Toggle
        =================================*/

        const filterToggle = document.querySelector('.filter_button');
        
        if(filterToggle) {
           
           filterToggle.addEventListener('click', (e) => {
            
                const filterBar = filterToggle.closest('.filter_bar');

                filterBar.classList.toggle('open');

            });
         
        }

	});

})(jQuery);