(function($) {

	$(document).ready(function() {

	"use strict";
        
        /*================================= 
        Tabify
        =================================*/
        
        $('.tabs_container').tabify({
                speed: 200
        });
        
        /*================================= 
        INSTAFEED
        http://instafeedjs.com/
        =================================*/

        var rnyInstafeed = new Instafeed({
                clientId: 'eb2a843725cc43d78edc8849b54e1851',
                accessToken: '432846995.eb2a843.bd26d5a9a6d94e0fbebd3c0cd5d98677',
                get: 'user',
                userId: '432846995',
                template: '<div class="instafeed_item_container"><a href="{{link}}" target="_blank" class="instafeed_item" style="background-image: url({{image}});"></a></div>',
                limit: 8,
                resolution: 'standard_resolution'
        });

        if($('#instafeed').length > 0) {
                rnyInstafeed.run();
        }
                		
	/*================================= 
        MAGNIFIC
        http://dimsemenov.com/plugins/magnific-popup/documentation.html
        =================================*/

        $('.wp_custom_gallery').each(function () {
                $(this).magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        titleSrc: 'title',
                        // Class that is added to popup wrapper and background
                        // make it unique to apply your CSS animations just to this exact popup
                        mainClass: 'mfp-fade',
                        gallery: {
                                enabled: true,
                                arrowMarkup: '<button title="%title%" type="button" class="lightbox_arrow lightbox_arrow_%dir%"></button>', // markup of an arrow button
                                tPrev: 'Previous (Left arrow key)', // title for left button
                                tNext: 'Next (Right arrow key)', // title for right button
                                tCounter: '<span class="mfp-counter">%curr% of %total%</span>' // markup of counter
                        }
                });
        });

	});

})(jQuery);