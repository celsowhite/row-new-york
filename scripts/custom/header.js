(function($) {

	$(document).ready(function() {

	"use strict";

		/*================================= 
		Elements
		=================================*/

		const mainHeader = document.querySelector('.main_header');

		const mainHeaderMenuIcon = document.querySelector('.main_header .menu_icon_container'); 
		
		const navigationOverlay = document.querySelector('.navigation_overlay');
		
		const mobileNavigationOverlay = document.querySelector('.mobile_navigation_overlay');
		
		const tabletMQ = window.matchMedia("(max-width: 768px)");
			
		/*================================= 
		Main Navigation Reveal
		=================================*/

		mainHeaderMenuIcon.addEventListener('click', () => {
			mainHeader.classList.toggle('navigation_open');
			mobileNavigationOverlay.classList.toggle('open');
			navigationOverlay.classList.toggle('open');
		});
		
		/*================================= 
		Header Scrolled
		=================================*/
		
		window.addEventListener('scroll', _.debounce(() => {
				
			// If scrolled 30px down the page then indicate the header has been scrolled.
			
			if(window.scrollY > 30) {
				mainHeader.classList.add('scrolled');
			}
			
			// Else return header to normal sizing.
			
			else {
				mainHeader.classList.remove('scrolled');
			}
				
		}, 30, {
			'leading': true
		}));
		
		/*================================= 
		Sub Navigation 
		=================================*/
		
		// Elements
		
		const mainHeaderSubNavContainer = document.querySelector('.main_header_sub_nav');
		
		const mainHeaderSubNav = document.querySelector('.main_header_sub_nav .sub_nav');
		
		const pageSubNav = document.querySelector('.page_header .sub_nav');
		
		const pageSections = document.querySelectorAll('.rny_page_section');
				
		// Check which section we are currently on and update the corresponding navigation item

		function checkSection() {
			
			Array.from(pageSections).forEach(section => {
				
				// Calculate the positioning of the section

				const height = section.offsetHeight;
				const topPosition = section.offsetTop;
				const bottomPosition = topPosition + height;

				// Find the sections corresponding link

				const sectionName = section.getAttribute('id');
				const sectionNavLink = document.querySelector('.main_header_sub_nav li[data-target="' + sectionName + '"]');

				// If scroll position is within current section then indicate it in the navigation

				const scrollBelowSectionTop = window.scrollY >= topPosition - 200;
				const scrollAboveSectionBottom = window.scrollY < bottomPosition - 200;

				if(scrollBelowSectionTop && scrollAboveSectionBottom) {
					sectionNavLink.classList.add('active');
				}
				else {
					sectionNavLink.classList.remove('active');
				}

			});
			
		};
		
		// If a page sub nav is on the page then set it's nav items and reveal on scroll.
		
		if(pageSubNav) {
			
			// Cache Measurements
		
			const pageSubNavPosition = pageSubNav.offsetTop;
			
			// Get sub nav elements from page header and set them in the main header.
			
			mainHeaderSubNav.innerHTML = pageSubNav.innerHTML;
			
			// Reveal Sub Nav in header once scroll past the pages static sub nav
		
			window.addEventListener('scroll', _.debounce(() => {
				
				if(window.scrollY > pageSubNavPosition) {
					mainHeaderSubNavContainer.classList.add('reveal');
				}
				else {
					mainHeaderSubNavContainer.classList.remove('reveal');
				}
				
				checkSection();
				
			}, 30));

		};
				
	});

})(jQuery);