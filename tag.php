<?php
/**
 * Tag Template
 */

get_header(); ?>

	<main class="main_wrapper">
		
		<?php
		// Save information about this term.
		$term = get_queried_object();
        $term_id = $term->term_id;
		$term_slug = $term->slug;
		$term_name = $term->name;
		?>
		        			
		<?php if ( have_posts() ) : ?>
			
			<header class="page_header dark">
				<div class="container">
					<h1>Blog</h1>
				</div>
			</header>
			
			<?php get_template_part('template-parts/component', 'tag_filter_bar'); ?>
			
			<div class="page_content">
				
				<div class="rny_panel">
                    
                    <div class="container">
                        
						<!-- First Post -->
						
                        <?php $post_count = 1; while ( have_posts() ) : the_post(); ?>

							<?php if($post_count === 1): ?>
																
								<?php get_template_part('template-parts/card', 'featured_post'); ?>
										
							<?php break; endif; ?>

						<?php $post_count++; endwhile; ?>
                        
						<!-- Post Grid -->
						
						<?php
                        // Ajax loaded posts
                        echo do_shortcode('[ajax_load_more 
                                            button_label="<a class=\'rny_button yellow\'>Load More</a>"
                                            button_loading_label="<a class=\'rny_button yellow\'>Loading</a>"
											tag="' . $term_slug . '"
                                            theme_repeater="card-blog_post.php" 
											transition_container_classes="blog_grid"
                                            scroll="false"
                                            posts_per_page="30"
											offset="1"]'
                                        ); 
                        ?>
						                        
                    </div>
                    
                </div>
				                                                            
			</div>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

	</main>

<?php get_footer(); ?>