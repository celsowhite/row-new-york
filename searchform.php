<form role="search" method="get" class="rny_search_form" action="<?php echo home_url( '/' ); ?>">
    <button type="submit" value="→" />
        <i class="far fa-search"></i>
    </button>
    <input type="search" placeholder="search" value="<?php echo get_search_query() ?>" name="s" title="Search" />
</form>