<?php
/**
* Product Category Template
 */

get_header(); ?>
	
	<main class="main_wrapper">

		<?php
		$shop_page_query = get_shop_page();
		while( $shop_page_query->have_posts()): $shop_page_query->the_post(); ?>
			<?php get_template_part('template-parts/component', 'page_header'); ?>
		<?php endwhile; wp_reset_postdata(); ?>
			                        
		<div class="page_content">

			<?php while( $shop_page_query->have_posts()): $shop_page_query->the_post(); ?>
				<?php get_template_part('template-parts/component', 'page_hero'); ?>
			<?php endwhile; wp_reset_postdata(); ?>
					
			<?php get_template_part('template-parts/component', 'shop_filter_bar'); ?>

			<div class="large_container">

				<div class="rny_panel">
					
					<div class="rny_row">
					
						<?php while ( have_posts() ) : the_post(); ?>

							<div class="column_1_3">
								<?php get_template_part('template-parts/woocommerce', 'product_thumbnail_card'); ?>
							</div>

						<?php endwhile; ?>

					</div>
				
				</div>

			</div>

		</div>

	</main>

<?php get_footer(); ?>