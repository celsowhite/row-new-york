<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

</div>

<div class="footer_container">
	
	<?php if(get_field('show_footer_call_to_action') && !is_single()): ?>
	
		<?php 
		// Conditionally set the color scheme.
		if(get_field('footer_cta_bar_color', 'option') == 'black') {
			$bar_color = 'black';
			$button_color = 'yellow_border';
		}
		else if(get_field('footer_cta_bar_color', 'option') == 'yellow') {
			$bar_color = 'yellow';
			$button_color = 'black';
		}
		else if(get_field('footer_cta_bar_color', 'option') == 'blue') {
			$bar_color = 'blue';
			$button_color = 'white_border';
		}
		?>

		<div class="call_to_action_bar <?php echo $bar_color; ?>">
			<h4><?php the_field('footer_cta_bar_text', 'option'); ?></h4>
			<a href="<?php the_field('footer_cta_bar_button_link', 'option'); ?>" class="rny_button <?php echo $button_color; ?>"><?php the_field('footer_cta_bar_button_text', 'option'); ?></a>
		</div>
	
	<?php endif; ?>
	
	<!-- Instagram Feed -->

	<section class="instagram_feed">
		<header class="section_header">
			<h4><?php the_field('instagram_feed_title', 'option'); ?></h4>
		</header>
		<div class="instafeed_container" id="instafeed"></div>
	</section>
	
	<footer class="footer">

		<div class="large_container">
		
			<div class="rny_row footer_columns">
				
				<?php $i=0; while(have_rows('footer_content_columns', 'option')): the_row(); ?>
			
					<div class="column_1_4">
						<div class="footer_column_content">
							<h4><?php the_sub_field('title'); ?></h4>
							<p><?php the_sub_field('content'); ?></p>
							<?php if($i === 0): ?>
								<a class="translate_toggle_container light modal_trigger" data-modal="translate_modal">
									<span>translate</span>
									<div class="translate_toggle">
										<div class="translate_toggle_circle"></div>
									</div>
								</a>
							<?php endif; ?>
						</div>
						<?php if($i === 0): ?>
							<img src="<?php echo get_template_directory_uri() . '/img/logo/logo_white.svg'; ?>" class="logo" />
						<?php endif; ?>
					</div>
				
				<?php $i++; endwhile; ?>
				
				<div class="column_1_4">
					<div class="footer_column_content">
						<h4>Connect</h4>
						
						<!-- Email Signup HTML -->

						<div class="mc_embed_signup">
							<form action="https://rownewyork.us12.list-manage.com/subscribe/post-json?u=ff88d7b4c6e6331122fa636f1&amp;id=3d4254d6e4&c=?" method="post" name="mc-embedded-subscribe-form" class="validate mc-embedded-subscribe-form trago_mailchimp_form" novalidate>
							<input type="email" value="" name="EMAIL" class="required email" placeholder="Enter your email" id="mce-EMAIL">
							<button type="submit" value="Submit" name="submit" id="mc-embedded-subscribe" class="button">
								<img src="<?php echo get_template_directory_uri() . '/img/icons/arrow_right.svg'; ?>" />
							</button>
							<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bfaeb9f9cfe94bf88ebe82105_58d7ecad1c" tabindex="-1" value=""></div>
							</form>
							<div class="mce-responses" class="clear">
							<div class="response mce-error-response" style="display:none"></div>
							<div class="response mce-success-response" style="display:none"></div>
							</div>
						</div>
						
						<!-- Social Profiles -->
						
						<ul class="social_icons flush_left">
							<?php if(have_rows('social_profiles', 'option')): while(have_rows('social_profiles', 'option')): the_row(); ?>
								<li>
									<?php if(get_sub_field('network', 'option') === 'fal fa-envelope'): ?>
										<a href="mailto:<?php the_sub_field('link'); ?>" target="_blank">
											<i class="<?php the_sub_field('network'); ?>"></i>
										</a>
									<?php else: ?>
										<a href="<?php the_sub_field('link', 'option'); ?>" target="_blank">
											<i class="<?php the_sub_field('network', 'option'); ?>"></i>
										</a>
									<?php endif; ?>
								</li>
							<?php endwhile; endif; ?>
						</ul>

					</div>
				</div>
				
			</div>
		
		</div>

	</footer>
	
</div>

<?php wp_footer(); ?>

<script>

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-45152948-1', 'auto');
	ga('send', 'pageview');

</script>

</body>
</html>
