<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="shortcut icon" type="image/png" href="<?php echo get_bloginfo('wpurl') . '/favicon.ico'; ?>" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php
	// Conditional set the header color.
	if(is_front_page()) {
		$header_color = 'transparent transparent_dark_background';
	}
	else if(is_page_template('page-templates/donate.php')) {
		$header_color = 'transparent transparent_light_background';
	}
	else if(is_tax('product_cat')) {
		$header_color = 'light';
	}
	else if(get_field('page_header_color')) {
		$header_color = get_field('page_header_color');
	}
	else {
		$header_color = 'dark';
	}
	?>

	<header class="main_header <?php echo $header_color; ?>">
	
		<div class="main_header_top">
			<div class="large_container">
				<div class="main_header_left">
					<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo get_template_directory_uri() . '/img/logo/logo_black.svg'; ?>" class="logo_black" />
						<img src="<?php echo get_template_directory_uri() . '/img/logo/logo_white.svg'; ?>" class="logo_white" />
					</a>
					<a class="translate_toggle_container modal_trigger" data-modal="translate_modal">
						<span>translate</span>
						<div class="translate_toggle">
							<div class="translate_toggle_circle"></div>
						</div>
					</a>
				</div>
				<div class="main_header_middle">
					<div class="menu_icon_container">
						<div class="menu_icon">
							<span class="menu_icon_line top"></span>
							<span class="menu_icon_line middle"></span>
							<span class="menu_icon_line bottom"></span>
						</div>
						<div class="menu_icon_text">menu</div>
					</div>
				</div>
				<div class="main_header_right">
					<?php
					$cart_count = WC()->cart->get_cart_contents_count();
					?>
					<a class="header_cart_icon <?php if($cart_count <= 0): ?>hide<?php endif; ?>" href="<?php echo wc_get_cart_url(); ?>">
						<i class="fas fa-shopping-cart"></i>
						<span class="cart_icon_number"><?php echo $cart_count; ?></span>
					</a>
					<a class="header_button" href="<?php echo page_link_by_slug('donate'); ?>">Donate</a>
				</div>
			</div>
		</div>
		
		<div class="main_header_sub_nav">
			<ul class="sub_nav"></ul>
		</div>
				
	</header>
	
	<?php get_template_part('template-parts/navigation', 'desktop_overlay'); ?>
		
	<?php get_template_part('template-parts/navigation', 'mobile_overlay'); ?>
	
	<?php get_template_part('template-parts/modal', 'video'); ?>
	
	<?php get_template_part('template-parts/modal', 'content'); ?>
	
	<?php get_template_part('template-parts/modal', 'translate'); ?>

	<div id="content" class="main_content">