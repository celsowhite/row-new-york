<?php
/**
 * The template for displaying all single program pages.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
        
            <?php 
            $page_header_link = page_link_by_slug('programs');
            $page_header_link_text = 'View All Programs';
            include(locate_template('template-parts/component-page_header.php')); ?>
			                        
			<div class="page_content">
                
                <?php get_template_part('template-parts/component', 'page_hero'); ?>
            
                <!-- Program Info -->
                                
				<div class="rny_panel">
					<div class="container">
						<div class="rny_row">
							<div class="column_1_3">
                                <?php if(get_field('sidebar_show_primary_button')): ?>
                                    <p><a href="<?php the_field('sidebar_primary_button_link'); ?>" class="rny_button yellow" <?php if(get_field('sidebar_primary_button_new_window')): ?>target="_blank"<?php endif; ?>><?php the_field('sidebar_primary_button_text'); ?></a></p>
                                <?php endif; ?>
                                <div class="wysiwyg">
                                    <?php the_field('sidebar_content'); ?>
                                </div>
							</div>
							<div class="column_2_3 wysiwyg tablet_flex_order_first">
                                <?php if(get_field('sidebar_primary_button_text')): ?>
                                    <div class="show_on_tablet">
                                        <p><a href="<?php the_field('sidebar_primary_button_link'); ?>" class="rny_button yellow" <?php if(get_field('sidebar_primary_button_new_window')): ?>target="_blank"<?php endif; ?>><?php the_field('sidebar_primary_button_text'); ?></a></p>
                                    </div>
                                <?php endif; ?>
                                <div class="program_card_table">
                                    <div class="program_card_table_column">
                                        <p class="small">Grades/Experience</p>
                                        <h4><?php the_field('program_grades'); ?></h4>
                                    </div>
                                    <div class="program_card_table_column">
                                        <p class="small">Duration</p>
                                        <h4><?php the_field('program_duration'); ?></h4>
                                    </div>
                                    <div class="program_card_table_column">
                                        <p class="small">Commitment</p>
                                        <h4><?php the_field('program_commitment'); ?></h4>
                                    </div>
                                </div>
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
                
                <?php get_template_part('template-parts/acf', 'page_components'); ?>
                
                <!-- Program Related Posts -->
                
                <?php get_template_part('template-parts/component', 'featured_category_posts'); ?>
                
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>