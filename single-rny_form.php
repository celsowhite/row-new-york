<?php
/**
 * The template for displaying all single forms.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('template-parts/component', 'page_header'); ?>

			<div class="page_content">
				<div class="rny_panel">
					<div class="container">
                        <div class="wysiwyg">
                            <?php the_content(); ?>
                        </div>
					</div>
				</div>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>