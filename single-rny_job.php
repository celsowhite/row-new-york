<?php
/**
 * The template for displaying all single job posts.
 */

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page_header dark">
				<div class="container">
					<h1><?php the_title(); ?></h1>
					<p class="page_header_link">
                        <a href="<?php echo page_link_by_slug('careers'); ?>">View All Careers</a>
                    </p>
				</div>
			</header>

			<div class="page_content">
				<div class="rny_panel">
					<div class="container">
						<div class="rny_row">
						
							<!-- Sidebar -->
							
							<div class="column_1_3 wysiwyg">
								<?php if(get_field('job_primary_button_text')): ?>
                                    <p><a href="<?php the_field('job_primary_button_link'); ?>" class="rny_button yellow"><?php the_field('job_primary_button_text'); ?></a></p>
                                <?php endif; ?>
                                                                
                                <?php if(get_field('job_type')): ?>
                                    <h4>Job Type</h4>
                                    <p><?php the_field('job_type'); ?></p>
                                <?php endif; ?>
                                
                                <?php if(get_field('job_supervisor')): ?>
                                    <h4>Supervisor</h4>
                                    <p><?php the_field('job_supervisor'); ?></p>
                                <?php endif; ?>
								
								<?php if(get_field('job_direct_reports')): ?>
                                    <h4>Direct Reports</h4>
                                    <p><?php the_field('job_direct_reports'); ?></p>
                                <?php endif; ?>
								
								<?php if(get_field('job_apply_instructions')): ?>
                                    <h4>Apply</h4>
                                    <p><?php the_field('job_apply_instructions'); ?></p>
                                <?php endif; ?>
								
							</div>
							
							<!-- Job Content -->
							
							<div class="column_2_3 wysiwyg">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>