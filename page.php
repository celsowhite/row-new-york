<?php
/*
Default page template
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part('template-parts/component', 'page_header'); ?>
			                        
			<div class="page_content">
                
                <?php get_template_part('template-parts/component', 'page_hero'); ?>
				
				<?php if(!empty(get_the_content())): ?>
					<div class="rny_panel">
						<div class="small_container wysiwyg">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endif; ?>
                
                <?php get_template_part('template-parts/acf', 'page_components'); ?>
                                            
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

