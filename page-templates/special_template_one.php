<?php
/*
Template Name: Special Template #1
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
            
            <header class="page_header <?php the_field('page_header_color'); ?>">
                <div class="container">
                    <?php if(get_field('page_header_subtitle')): ?>
                        <p><?php the_field('page_header_subtitle'); ?></p>
                    <?php endif; ?>
                    <h1><?php the_title(); ?></h1>
                </div>
            </header>
                        
			<div class="page_content">
                
                <?php get_template_part('template-parts/component', 'page_hero'); ?>
                                
                <div class="rny_panel large">
                    
                    <div class="container">
                        <h2><?php the_field('featured_text'); ?></h2>
                        <div class="rny_panel padding_top">
                            <div class="rny_row">
                                <div class="column_2_3">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="special_template_featured_video">
                        <div class="video_image">
                            <img src="<?php echo image_id_to_url(get_field('featured_side_image'), 'large'); ?>" />
                        </div>
                        <?php if(get_field('show_featured_video')): ?>
                            <a class="modal_trigger video_info_box left" data-modal="video_modal" data-video-type="<?php the_field('featured_video_type'); ?>" data-video-url="<?php the_field('featured_video_embed', false, false); ?>">
                                <div class="video_info">
                                    <h4><?php the_field('featured_video_title'); ?></h4>
                                    <p><?php the_field('featured_video_time'); ?></p>
                                </div>
                                <div class="play_button">
                                    <div class="triangle"></div>
                                </div>
                            </a>
                        <?php endif; ?>
                    </div>

                </div>
                
                <?php get_template_part('template-parts/acf', 'page_components'); ?>
                                            
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
