<?php
/*
Template Name: Staff
*/

get_header(); ?>

	<main class="main_wrapper mixitup_container">

		<?php while ( have_posts() ) : the_post(); ?>
			
			<header class="page_header light">
				<div class="container">
                    
					<h1><?php the_title(); ?></h1>
                    
                    <!-- Staff Location Filters -->
                    
                    <ul class="sub_nav mixitup_filters">
                        <li class="filter" data-filter="all">All</li>
                        <?php
                        $terms = get_terms( array(
                            'taxonomy'   => 'rny_staff_location',
                            'hide_empty' => false,
                        ));
                        foreach($terms as $term):
                        ?>
                            <li class="filter" data-filter="<?php echo '.' . $term->slug ?>"><?php echo $term->name; ?></li>
                        <?php endforeach; ?>
                    </ul>
                    
				</div>
			</header>
			                        
			<div class="page_content">
            
                <!-- Staff Grid -->
                
                <ul class="staff_grid">				
                    <?php
                    $staff_loop_args = array (
                        'post_type'         => 'rny_staff', 
                        'posts_per_page'    => -1,
                        'orderby'			=> 'name',
                        'order'				=> 'ASC'
                    );
                    $staff_loop = new WP_Query($staff_loop_args);
                    if ($staff_loop -> have_posts()) : while ($staff_loop -> have_posts()) : $staff_loop -> the_post();
                    ?>
                        <?php 
                        // Save locations for this staff member so we can output them onto their card for mixitup filtering
                        $locations = get_the_terms($post->ID, 'rny_staff_location'); 
	                    $location_slugs = wp_list_pluck($locations, 'slug');
	                    $mixitup_locations = join(' ', $location_slugs);
                        ?>
                        
                        <li class="staff_thumbnail_card mix <?php echo $mixitup_locations; ?>">
                            <?php 
                            // Dynamically set the featured image
                            if(has_post_thumbnail()) {
                                $featured_image = get_the_post_thumbnail_url($post->ID, 'medium');
                            }
                            else {
                                $featured_image = image_id_to_url(get_field('default_staff_image', 'option'), 'medium');
                            }
                            // Hover Image
                            $staff_image_hover = image_id_to_url(get_field('staff_image_hover'), 'medium');
                            ?>
                            <div 
                            class="thumbnail_card_image staff_thumbnail_card_image"
                            style="background-image: url('<?php echo $featured_image; ?>');"
                            >
                                <?php if($staff_image_hover): ?>
                                    <div class="thumbnail_card_image_hover" style="background-image: url('<?php echo $staff_image_hover; ?>');"></div>
                                <?php endif; ?>
                            </div>
                            <div class="staff_thumbnail_card_content">
                                <h4><?php the_title(); ?></h4>
                                <?php if(get_field('staff_title')): ?>
                                    <p><?php the_field('staff_title'); ?></p>
                                <?php endif; ?>
                                <?php if(get_field('staff_email')): ?>
                                    <p><a href="mailto:<?php the_field('staff_email'); ?>"><?php the_field('staff_email'); ?></a></p>
                                <?php endif; ?>
                                <?php if(get_field('staff_link')): ?>
                                    <p class="wysiwyg"><a class="staff_thumbnail_card_link" href="<?php the_field('staff_link'); ?>">Learn More</a></p>
                                <?php endif; ?>
                                <?php 
                                $social_icon_alignment = 'center';
                                include(locate_template('template-parts/component-social_profiles.php')); ?>
                            </div>
                        </li>
                    <?php endwhile; wp_reset_postdata(); endif; ?>
                </ul>
                                                            
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>