<?php
/*
Template Name: Programs
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
			
			<header class="page_header light">
				<div class="container">
					<h1><?php the_title(); ?></h1>
                    <ul class="sub_nav">
                        <?php
                        // Program type sub navigation
                        $terms = get_terms( array(
                            'taxonomy'   => 'rny_program_type',
                            'hide_empty' => false,
                        ));
                        foreach($terms as $term):
                        ?>
                            <li class="smooth_scroll" data-target="<?php echo $term->slug; ?>"><?php echo $term->name; ?></li>
                        <?php endforeach; ?>
                    </ul>
				</div>
			</header>
			                        
			<div class="page_content">
                                    
                <?php
                // Loop through each of the program types and output the programs within them.
                $terms = get_terms( array(
                    'taxonomy'   => 'rny_program_type',
                    'hide_empty' => false,
                ));
                foreach($terms as $term):
                ?>
                    
                    <header class="section_header">
                        <h4><?php echo $term->name; ?></h4>
                    </header>
                    
                    <div class="rny_page_section" id="<?php echo $term->slug; ?>">
                    
                        <?php
                        $program_count = 0;
                        // Program list
                        $program_loop_args = array (
                            'post_type' => 'rny_program', 
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'rny_program_type',
                                    'field'    => 'slug',
                                    'terms'    => $term->slug,
                                ),
                            ),
                            'posts_per_page'  => -1
                        );
                        $program_loop = new WP_Query($program_loop_args);
                        if ($program_loop -> have_posts()) : while ($program_loop -> have_posts()) : $program_loop -> the_post();
                        ?>
                            
                            <div class="program_panel">
                                
                                <?php
                                // Load program image with responsive background image technique.
                                $featured_image_horizontal_focal_point = get_field('featured_image_horizontal_focal_point'); 
				                $featured_image_vertical_focal_point = get_field('featured_image_vertical_focal_point');
                                ?>
                                
                                <a 
                                href="<?php the_permalink(); ?>" 
                                class="program_panel_image <?php echo $term->slug . '_image_' . $program_count ?>"
                                style="background-position: <?php echo $featured_image_horizontal_focal_point . ' ' . $featured_image_vertical_focal_point; ?>"></a>

                                <style>                                    
                                    .<?php echo $term->slug; ?>_image_<?php echo $program_count ?> {
                                        background-image: url('<?php the_post_thumbnail_url('large'); ?>');
                                    }
                                    @media all and (max-width: 1024px) {
                                        .<?php echo $term->slug; ?>_image_<?php echo $program_count ?> {
                                            background-image: url('<?php the_post_thumbnail_url('medium'); ?>');
                                        }
                                    }
                                    @media all and (max-width: 568px) {
                                        .<?php echo $term->slug; ?>_image_<?php echo $program_count ?> {
                                            background-image: url('<?php the_post_thumbnail_url('medium_large'); ?>');
                                        }
                                    }
                                </style>
                                
                                <div class="program_panel_content">
                                    <div class="program_panel_card">
                                        <header class="program_card_header">
                                            <?php if(get_field('program_enrollment')): ?>
                                                <p class="small"><?php the_field('program_enrollment'); ?></p>
                                            <?php endif; ?>
                                            <h3><?php the_title(); ?></h3>
                                        </header>
                                        <div class="program_card_table">
                                            <div class="program_card_table_column">
                                                <p class="small">Grades/Experience</p>
                                                <h4><?php the_field('program_grades'); ?></h4>
                                            </div>
                                            <div class="program_card_table_column">
                                                <p class="small">Duration</p>
                                                <h4><?php the_field('program_duration'); ?></h4>
                                            </div>
                                            <div class="program_card_table_column">
                                                <p class="small">Commitment</p>
                                                <h4><?php the_field('program_commitment'); ?></h4>
                                            </div>
                                        </div>
                                        <div class="program_card_excerpt">
                                            <?php the_excerpt(); ?>
                                        </div>
                                        <a href="<?php the_permalink(); ?>" class="rny_button yellow">Learn More</a>
                                    </div>
                                </div>
                            
                            </div>

                        <?php $program_count++; endwhile; wp_reset_postdata(); endif; ?>
                        
                    </div>

                <?php endforeach; ?>
                                                                                                        
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

