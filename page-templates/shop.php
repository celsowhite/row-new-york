<?php
/*
Template Name: Shop
*/

get_header(); ?>

	<main class="main_wrapper mixitup_container">

		<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part('template-parts/component', 'page_header'); ?>
			                        
			<div class="page_content">

                <?php get_template_part('template-parts/component', 'page_hero'); ?>
                
                <?php get_template_part('template-parts/component', 'shop_filter_bar'); ?>

                <div class="large_container">

                    <div class="rny_panel">

                        <!-- Product Grid -->
                        
                        <div class="rny_row">				
                            <?php
                            $product_loop_args = array (
                                'post_type'         => 'product', 
                                'posts_per_page'    => -1,
                                // Hide products that are marked as 'Exclude From Catalog'
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_visibility',
                                        'field'    => 'name',
                                        'terms'    => 'exclude-from-catalog',
                                        'operator' => 'NOT IN',
                                    ),
                                )
                            );
                            $product_loop = new WP_Query($product_loop_args);
                            if ($product_loop -> have_posts()) : while ($product_loop -> have_posts()) : $product_loop -> the_post();
                            ?>                        
                                <div class="column_1_3">
                                    <?php get_template_part('template-parts/woocommerce', 'product_thumbnail_card'); ?>
                                </div>
                            <?php endwhile; wp_reset_postdata(); endif; ?>

                        </div>
                    
                    </div>

                </div>

                <?php get_template_part('template-parts/acf', 'page_components'); ?>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>