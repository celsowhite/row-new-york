<?php
/*
Template Name: Events
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
			
            <?php get_template_part('template-parts/component', 'page_header'); ?>
			                        
			<div class="page_content">
                
                <?php

                // Set timezone to NY
                
				date_default_timezone_set('America/New_York');

                // Save yesterdays date so we can keep events on the site until after their start/end date has passed.
                
                $date_now = time();
                $date_yesterday = date('Y-m-d H:i:s', strtotime('-1 day', $date_now));

                // Query all upcoming events.
                // Order the events by ascending start date.
                
                $event_loop_args = array (
                    'post_type' => 'rny_event', 
                    'posts_per_page' => -1, 
                    // Meta query posts whose start date hasn't passed or if the end date exists and hasn't passed.
                    'meta_query'     => array(
                        'relation'      => 'OR',
                        array(
                            'key'       => 'event_start_date',
                            'value'     => $date_yesterday,
                            'compare'   => '>',
                            'type'      => 'DATE'
                        ),
                        array(
                            'relation'  => 'AND',
                            array(
                                'key'       => 'event_end_date',
                                'compare'   => 'EXISTS'
                            ),
                            array(
                                'key'       => 'event_end_date',
                                'value'     => $date_yesterday,
                                'compare'   => '>',
                                'type'      => 'DATE'
                            )
                        )
                    ),
                    // Order the results of the query by ascending event start date.
                    'order'           => 'ASC',
                    'orderby'         => 'meta_value',
                    'meta_key'        => 'event_start_date',
                    'meta_type'       => 'DATETIME'
                );
                $event_loop = new WP_Query($event_loop_args);
                if ($event_loop -> have_posts()):
                ?>
                    <?php while ($event_loop -> have_posts()) : $event_loop -> the_post(); ?>
                        <div class="event_panel">
                            <a class="event_panel_image" style="background-image:url('<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>')" href="<?php the_permalink(); ?>">
                                <h1 class="event_panel_date"><?php the_field('event_start_date'); ?><?php if(get_field('event_end_date')): ?> - <?php the_field('event_end_date'); ?><?php endif; ?></h1>
                            </a>
                            <div class="event_panel_content">
                                <div class="container">
                                    <div class="rny_row">
                                        <div class="column_1_2 wysiwyg">
                                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                        </div>
                                        <div class="column_1_2">
                                            <?php the_excerpt(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else: ?>
                    <div class="rny_panel">
						<div class="small_container wysiwyg">
							<?php the_content(); ?>
						</div>
					</div>
                <?php endif; ?>
                                            
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>