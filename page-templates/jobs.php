<?php
/*
Template Name: Jobs
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part('template-parts/component', 'page_header'); ?>
			                        
			<div class="page_content">
                
                <?php get_template_part('template-parts/component', 'page_hero'); ?>
				
				<?php if(!empty(get_the_content())): ?>
					<div class="rny_panel">
						<div class="small_container wysiwyg">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endif; ?>
                
                <div class="link_blocks three_columns black">
                    
                    <?php
                    // Jobs Listings
                    $job_loop_args = array ('post_type' => 'rny_job', 'posts_per_page' => -1);
                    $job_loop = new WP_Query($job_loop_args);
                    if ($job_loop -> have_posts()) : while ($job_loop -> have_posts()) : $job_loop -> the_post();
                    ?>
                        <a class="link_block" href="<?php the_permalink(); ?>">
                            <div class="content">
                                <h3><?php the_title(); ?></h3>
                                <span class="separator"></span>
                                <?php if(has_excerpt()): ?>
                                    <p><?php the_excerpt(); ?></p>
                                <?php endif; ?>
                            </div>
                        </a>
                    <?php endwhile; endif; ?>
                    
                </div>
                
                <?php get_template_part('template-parts/acf', 'page_components'); ?>
                                            
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>

