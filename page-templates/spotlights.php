<?php
/*
Template Name: Spotlights
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part('template-parts/component', 'page_header'); ?>
			                        
			<div class="page_content spotlight_page_content">
                                
                <?php
                $spotlight_loop_args = array ('post_type' => 'rny_spotlight', 'posts_per_page' => -1);
                $spotlight_loop = new WP_Query($spotlight_loop_args);
                if ($spotlight_loop -> have_posts()): 
                ?>
                
                    <div class="tabs_container">
                        
                        <!-- Thumbnail Slider -->
                        
                        <div class="flexslider thumbnail_slider">
                            <ul class="slides tab_items">
                                <?php $i=0; while ($spotlight_loop -> have_posts()) : $spotlight_loop -> the_post(); ?>
                                    <li data-title="slider_tab_<?php echo $i; ?>">
                                        <div class="thumbnail_slider_overlay">
                                            <div class="content">
                                                <h3><?php the_title(); ?></h3>
                                                <?php 
                                                $spotlight_info = array(get_field('spotlight_graduating_class'), get_field('spotlight_college'));
                                                $filtered_spotlight_info = array_filter($spotlight_info, function($value) { return $value !== ''; });
                                                if($filtered_spotlight_info): ?>
                                                    <p><?php echo implode(' · ', $filtered_spotlight_info); ?></p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'rny_thumbnail'); ?>" />
                                    </li>
                                <?php $i++; endwhile; wp_reset_postdata(); ?>
                            </ul>
                        </div>
                    
                        <!-- Main Slider -->
                        
                        <div class="flexslider background_image_slider regular thumbnail_controlled_slider">
                            <ul class="slides">
                                <?php while ($spotlight_loop -> have_posts()) : $spotlight_loop -> the_post(); ?>
                                    <li style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>)"></li>
                                <?php endwhile; wp_reset_postdata(); ?>
                            </ul>
                        </div>
                                            
                        <!-- Spotlight Content -->
                        
                        <div class="tab_content">
                            <?php $i=0; while ($spotlight_loop -> have_posts()) : $spotlight_loop -> the_post(); ?>
                                <div class="rny_panel" id="slider_tab_<?php echo $i; ?>">
                                    <div class="container">
                                        <div class="rny_row">
                                            <div class="column_1_3">
                                                
                                                <?php 
                                                $spotlight_info = array(get_field('spotlight_graduating_class'), get_field('spotlight_college'));
                                                $filtered_spotlight_info = array_filter($spotlight_info, function($value) { return $value !== ''; });
                                                if($filtered_spotlight_info): ?>
                                                    <p class="text_light margin_bottom_0"><?php echo implode(' · ', $filtered_spotlight_info); ?></p>
                                                <?php endif; ?>
                                                
                                                <h2><?php the_title(); ?></h2>
                                                
                                                <!-- Social Share -->
                                                
                                                <h4>Share</h4>
                                                
                                                <ul class="social_icons rny_social_share flush_left">
                                                    <?php get_template_part('template-parts/component', 'social_share'); ?>
                                                </ul>
                                                
                                            </div>
                                            <div class="column_2_3 wysiwyg">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php $i++; endwhile; wp_reset_postdata(); ?>
                        </div>
                    
                    </div>
                    
                <?php endif; ?>
                				                                                                            
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>