<?php
/*
Template Name: Blog
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
            
            <?php get_template_part('template-parts/component', 'page_header'); ?>
            
            <?php get_template_part('template-parts/component', 'blog_filter_bar'); ?>
            
			<div class="page_content">
                                
                <div class="rny_panel">
                    
                    <div class="container">
                        
                        <!-- Most Recent Post -->
                        
                        <?php
                        $blog_loop_args = array (
                            'post_type'        => 'post', 
                            'posts_per_page'   => 1
                        );
                        $blog_loop = new WP_Query($blog_loop_args);
                        if ($blog_loop -> have_posts()) : while ($blog_loop -> have_posts()) : $blog_loop -> the_post();
                        ?>
                        
                            <?php get_template_part('template-parts/card', 'featured_post'); ?>
                            
                        <?php endwhile; wp_reset_postdata(); endif; ?>
                        
                        <!-- Grid Of All Posts -->
                                         
                        <?php
                        // Ajax loaded posts
                        echo do_shortcode('[ajax_load_more 
                                            button_label="<a class=\'rny_button yellow\'>Load More</a>"
                                            button_loading_label="<a class=\'rny_button yellow\'>Loading</a>"
                                            theme_repeater="card-blog_post.php" 
                                            transition_container_classes="blog_grid"
                                            scroll="false"
                                            offset="1"
                                            posts_per_page="30"]'
                                        ); 
                        ?>
                        
                    </div>
                    
                </div>
                                                                        
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>