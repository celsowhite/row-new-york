<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content">
				<?php if( have_rows('fullscreen_slider') ): ?>
					<div class="flexslider fullscreen_slider">
						<ul class="slides"> 
							<?php $slide_count=0; while( have_rows('fullscreen_slider') ): the_row(); ?>
								<li class="fullscreen_slide_<?= $slide_count ?>">
									<div class="fullscreen_slider_content_container">
										<div class="large_container">
											<div class="fullscreen_slider_content">
												<h1><?php the_sub_field('slide_title'); ?></h1>
												<p><?php the_sub_field('slide_text'); ?></p>
												<a class="rny_button yellow" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('tab_title'); ?></a>
											</div>
										</div>
									</div>
								</li>
								<style>
									<?php if($slide_count === 0): ?>
										.fullscreen_slide_<?= $slide_count ?> {
											background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'large'); ?>');
										}
										@media all and (max-width: 1024px) {
											.fullscreen_slide_<?= $slide_count ?> {
												background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'medium'); ?>');
											}
										}
										@media all and (max-width: 568px) {
											.fullscreen_slide_<?= $slide_count ?> {
												background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'medium_large'); ?>');
											}
										}
									<?php else: ?>
										.fullscreen_slide_<?= $slide_count ?>.loaded {
											background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'large'); ?>');
										}
										@media all and (max-width: 1024px) {
											.fullscreen_slide_<?= $slide_count ?>.loaded {
												background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'medium'); ?>');
											}
										}
										@media all and (max-width: 568px) {
											.fullscreen_slide_<?= $slide_count ?>.loaded {
												background-image: url('<?php echo image_id_to_url(get_sub_field('image'), 'medium_large'); ?>');
											}
										}
									<?php endif; ?>
								</style>
							<?php $slide_count++; endwhile; ?>
						</ul>
					</div>
					<div class="fullscreen_slider_tabs_container">
						<div class="large_container">
							<ul class="fullscreen_slider_tabs">
								<?php while( have_rows('fullscreen_slider') ): the_row(); ?>
									<li>
										<a class="fullscreen_slider_tab" href="<?php the_sub_field('link'); ?>">	
											<h3><?php the_sub_field('tab_title'); ?></h3>
											<p><?php the_sub_field('tab_text'); ?></p>
										</a>
									</li>
								<?php endwhile; ?>
							</ul>
						</div>
					</div>
				<?php endif; ?>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
