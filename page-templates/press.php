<?php
/*
Template Name: Press
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
			
            <?php 
            include(locate_template('template-parts/component-page_header.php')); ?>
			                        
			<div class="page_content">
                
                <ul class="link_blocks black three_columns">			
                    <?php
                    // Set timezone to NY
                    date_default_timezone_set('America/New_York');

                    // Get the date/time for right now
                    $date_now = date('Y-m-d H:i:s');

                    $press_loop_args = array (
                        'post_type' => 'rny_press', 
                        'posts_per_page' => -1,
                        // Order the results of the query by ascending press date.
                        'order'           => 'DESC',
                        'orderby'         => 'meta_value_num',
                        'meta_key'        => 'press_publication_date'
                    );
                    $press_loop = new WP_Query($press_loop_args);
                    if ($press_loop -> have_posts()) : while ($press_loop -> have_posts()) : $press_loop -> the_post();
                    ?>
                        <a class="link_block with_background_image" href="<?php the_field('press_link'); ?>" target="_blank">
                            <?php if(has_post_thumbnail()): ?>
                                <div class="background_image" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'medium'); ?>"></div>
                            <?php endif; ?>
                            <div class="content">
                                <h3><?php the_title(); ?></h3>
                                <span class="separator"></span>
                                <p><?php the_field('press_publication'); ?></p>
                                <p>
                                    <?php 
                                    $publication_date = get_field('press_publication_date', false, false);
                                    $publication_date = new DateTime($publication_date);
                                    echo $publication_date->format('F j, Y'); 
                                    ?>
                                </p>
                            </div>
                        </a>
                    <?php endwhile; endif; ?>
                </ul>
                                            
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>