<?php
/*
Template Name: Donate
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part('template-parts/component', 'page_header'); ?>
			                        
			<div class="page_content">
                
                <!-- Top Donation Section -->
                				                
                <section class="donate_top">
                    
                    <style>
                        .donate_top {
                            background-image: url('<?php echo image_id_to_url(get_field('donate_background_image'), 'large'); ?>');
                        }
                        @media all and (max-width: 1024px) {
                            .donate_top {
                                background-image: url('<?php echo image_id_to_url(get_field('donate_background_image'), 'medium'); ?>');
                            }
                        }
                        @media all and (max-width: 568px) {
                            .donate_top {
                                background-image: url('<?php echo image_id_to_url(get_field('donate_background_image'), 'medium_large'); ?>');
                            }
                        }
                    </style>
                    
                    <div class="rny_panel large">
                        <div class="container">
                            <div class="rny_row">
                                
                                <!-- Donate Content -->
                                
                                <div class="column_1_2">
                                    <div class="wysiwyg">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                                
                                <!-- Donate Payment Methods & Logos -->
                                
                                <div class="column_1_2">
                                    
                                    <!-- Payment Methods -->
                                    
                                    <p><?php the_field('donate_payment_methods_header'); ?></p>
                                    
                                    <div class="tabs_container">
                                        <ul class="tab_items rny_pills flush_left">
                                            <?php $i=0; while(have_rows('donate_payment_methods')): the_row(); ?>
                                                <li data-title="payment_tab_<?php echo $i; ?>">
                                                    <?php the_sub_field('title'); ?>
                                                </li>
                                            <?php $i++; endwhile; ?>
                                        </ul>
                                        <div class="tab_content wysiwyg">
                                            <?php $i=0; while(have_rows('donate_payment_methods')): the_row(); ?>
                                                <div id="payment_tab_<?php echo $i; ?>">
                                                    <?php the_sub_field('content'); ?>
                                                </div>
                                            <?php $i++; endwhile; ?>
                                        </div>
                                    </div>
                                    
                                    <!-- Logos -->
                                    
                                    <ul class="logo_grid">
                                        <?php while(have_rows('donate_logos')): the_row(); ?>
                                            <li>
                                                <?php if(get_sub_field('link')): ?>
                                                    <a href="<?php the_sub_field('link'); ?>" target="_blank">
                                                        <img src="<?php echo image_id_to_url(get_sub_field('logo'), 'medium'); ?>" />
                                                    </a>
                                                <?php else: ?>
                                                    <img src="<?php echo image_id_to_url(get_sub_field('logo'), 'medium'); ?>" />
                                                <?php endif; ?>
                                            </li>
                                        <?php $i++; endwhile; ?>
                                    </ul>
                                    
                                </div>
                            </div>
                        </div>
					</div>
                
                </section>
                                
                <?php get_template_part('template-parts/acf', 'page_components'); ?>
                                            
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
