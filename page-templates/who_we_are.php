<?php
/*
Template Name: Who We Are
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
                        
            <header class="page_header light">
				<div class="container">
					<?php if(get_field('page_header_subtitle')): ?>
                        <p><?php the_field('page_header_subtitle'); ?></p>
                    <?php endif; ?>
                    <h1><?php the_title(); ?></h1>
                    <ul class="sub_nav">
                        <li class="smooth_scroll" data-target="founder">Founder</li>
                        <li class="smooth_scroll" data-target="board">Board</li>
                        <li class="smooth_scroll wwa-tab-trigger" data-target="additional-groups" data-tab-name="supporter">Partners</li>
                        <li class="smooth_scroll" data-target="staff">Staff</li>
                    </ul>
				</div>
			</header>
                        
			<div class="page_content">
                
                <?php get_template_part('template-parts/component', 'page_hero'); ?>
                
                <!-- Founder -->
                
                <div class="rny_page_section" id="founder">
                    
                    <div class="rny_panel">
                        <div class="container">
                            <div class="rny_row">
                                <div class="column_1_3">
                                    <img src="<?php echo image_id_to_url(get_field('founder_image'), 'large'); ?>" />
                                </div>
                                <div class="column_2_3 wysiwyg">
                                    <p class="text_light margin_bottom_0"><?php the_field('founder_subtitle'); ?></p>
                                    <h2><?php the_field('founder_title'); ?></h2>
                                    <?php the_field('founder_bio'); ?>
                                    <?php 
                                    $social_icon_alignment = 'flush_left';
                                    include(locate_template('template-parts/component-social_profiles.php')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
                
                <!-- Board -->
                
                <div class="rny_page_section" id="board">
                    
                    <header class="section_header">
                        <h4><?php the_field('board_title', 'option'); ?></h4>
                    </header>
                    
                    <!-- Board Grid -->
                
                    <ul class="board_grid">				
                        <?php
                        $board_loop_args = array ('post_type' => 'rny_board', 'posts_per_page' => -1);
                        $board_loop = new WP_Query($board_loop_args);
                        if ($board_loop -> have_posts()) : while ($board_loop -> have_posts()) : $board_loop -> the_post();
                        ?>                            
                            <li 
                            class="staff_thumbnail_card modal_trigger" 
                            data-modal="content_modal"
                            >
                                <div 
                                class="thumbnail_card_image staff_thumbnail_card_image"
                                style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, 'medium_large'); ?>');">
                                </div>
                                <div class="staff_thumbnail_card_content">
                                    <h4><?php the_title(); ?></h4>
                                    <p><?php the_field('board_title'); ?></p>
                                </div>
                                <div class="modal_content_to_show">
                                    <p class="text_light margin_bottom_0"><?php the_field('board_title'); ?></p>
                                    <h2><?php the_title(); ?></h2>
                                    <div class="wysiwyg">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </li>
                        <?php endwhile; wp_reset_postdata(); endif; ?>
                    </ul>

                </div>
                    
                <!-- Additional Groups -->

                <div class="rny_page_section" id="additional-groups">

                    <div class="rny_panel">
                        
                        <div class="container tabs_container">
                        
                            <div class="rny_row">
                                
                                <div class="column_1_2">
                                    
                                    <ul class="additional_groups_list tab_items">

                                        <?php $i=0; if(have_rows('additional_groups')): while(have_rows('additional_groups')): the_row(); ?>
                                            <li data-title="group_tab_<?php echo $i; ?>" data-name="<?php echo sanitize_title(get_sub_field('title')); ?>">
                                                <h3><?php the_sub_field('title'); ?></h3>
                                            </li>
                                        <?php $i++; endwhile; endif; ?>
                                        
                                    </ul>
                                    
                                </div>
                                
                                <div class="column_1_2">
                                                                        
                                    <div class="tab_content">
                                        
                                        <?php $i=0; if(have_rows('additional_groups')): while(have_rows('additional_groups')): the_row(); ?>
                                            <div id="group_tab_<?php echo $i; ?>">
                                                <?php the_sub_field('content'); ?>
                                            </div>
                                        <?php $i++; endwhile; endif; ?>
                                        
                                    </div>
                                    
                                </div>
                                
                            </div>
                        
                        </div>
                        
                    </div>
                    
                </div>
                
                <!-- Row NYC Staff -->
                
                <div class="rny_page_section" id="staff">
                
                    <header class="section_header">
                        <h4><?php the_field('staff_title', 'option'); ?></h4>
                    </header>
                    
                    <?php get_template_part('template-parts/acf', 'link_blocks_single'); ?>
                    
                </div>
                                                           
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>