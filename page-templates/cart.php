<?php
/*
Template Name: Cart
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
            
            <?php get_template_part('template-parts/component', 'page_header'); ?>
            
			<div class="page_content">
				
				<?php get_template_part('template-parts/component', 'page_hero'); ?>
				                
                <div class="rny_panel">
                    <div class="large_container">
                        <?php the_content(); ?>
                    </div>
                </div>
				
				<?php get_template_part('template-parts/acf', 'page_components'); ?>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>