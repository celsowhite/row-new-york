<?php
/*
Template Name: Page w/ Sidebar
*/

get_header(); ?>

	<main class="main_wrapper">

		<?php while ( have_posts() ) : the_post(); ?>
            
            <?php get_template_part('template-parts/component', 'page_header'); ?>
            
			<div class="page_content">
				
				<?php get_template_part('template-parts/component', 'page_hero'); ?>
				
				<div class="rny_panel">
					<div class="container">
						<div class="rny_row">
							<div class="column_1_3">
                                <div class="wysiwyg">
                                    <?php the_field('sidebar_content'); ?>
                                </div>
                                <?php if(get_field('sidebar_show_primary_button')): ?>
                                    <a href="<?php the_field('sidebar_primary_button_link'); ?>" class="rny_button yellow" <?php if(get_field('sidebar_primary_button_new_window')): ?>target="_blank"<?php endif; ?>><?php the_field('sidebar_primary_button_text'); ?></a>
                                <?php endif; ?>
							</div>
							<div class="column_2_3 wysiwyg">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
				
				<?php get_template_part('template-parts/acf', 'page_components'); ?>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
